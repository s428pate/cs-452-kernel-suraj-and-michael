#include "train_status.h"
#include "common.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "track_node.h"
#include "track_data_new.h"
#include "train_switch_server.h"
#include "timer.h"
#include "printf.h"

#define SENSOR_STORE 7

#define STRAIGHT_CODE 33
#define CURVED_CODE 34

#define TEXT_RED           "\033[31m"
#define TEXT_GREEN         "\033[32m"
#define TEXT_YELLOW        "\033[33m"
#define TEXT_BLUE          "\033[34m"
#define TEXT_MAGENTA       "\033[35m"
#define TEXT_CYAN          "\033[36m"
#define TEXT_WHITE         "\033[37m"
#define TEXT_BLACK         "\033[30m"

struct track_node track[144];
struct track_node *mytrack;


struct dijNode nodes[144];
struct dijNode *dijs[144];
int dijStart;
int dijEnd;
int reverseInsert[144];

struct dijNode *path[NUMTRAINS][144];
int pathLen[2];
int pathIndex[NUMTRAINS];
int allowedNodes[TRACK_MAX];
int allowedNodesLength;

static int isCalculating[NUMTRAINS];
static int isActive[NUMTRAINS];

static long int train_acceleration[NUMTRAINS][15];
static long int train_deceleration[NUMTRAINS][15];

static long int train_speed[NUMTRAINS][15];
static int train_current_speed[NUMTRAINS];
static int train_target_speed[NUMTRAINS];

static int trainNums[NUMTRAINS];
static char trainSpeeds[100];
static char switches[200];
static char passedSensors[SENSOR_STORE];
static char passedSensorAttributions[SENSOR_STORE];
static long int sensorIdx;

static char largeReply[MESSAGE_MAX_SIZE];
static char msg_buf[MESSAGE_MAX_SIZE];


char command_to_code(enum commands cmd) {
    if (cmd == COMMAND_CURVED) return CURVED_CODE;
    if (cmd == COMMAND_STRAIGHT) return STRAIGHT_CODE;
    return -1;
}

char switch_code_to_char(char code) {
    return (code == STRAIGHT_CODE) ? 'S' : 'C';
}

char code_to_bank(char code) {
    return 'A' + (code >> 4);
}

int code_to_socket(char code) {
    return (code & 0x0F) + 1;
}

void printSwitches(long int turnout) {

    switch ((turnout - 1) / 3) {
        case 0:
            Print("Switches: \033[33m[01: %c], [02: %c], [03: %c]", switch_code_to_char(switches[1]), switch_code_to_char(switches[2]), switch_code_to_char(switches[3]), print_switch_i);
            break;
        case 1:
            Print("          [04: %c], [05: %c], [06: %c]", switch_code_to_char(switches[4]), switch_code_to_char(switches[5]), switch_code_to_char(switches[6]), print_switch_i+3);
            break;
        case 2:
            Print("          [07: %c], [08: %c], [09: %c]", switch_code_to_char(switches[7]), switch_code_to_char(switches[8]), switch_code_to_char(switches[9]), print_switch_i+6);
            break;
        case 3:
            Print("          [10: %c], [11: %c], [12: %c]", switch_code_to_char(switches[10]), switch_code_to_char(switches[11]), switch_code_to_char(switches[12]), print_switch_i+9);
            break;
        case 4:
            Print("          [13: %c], [14: %c], [15: %c]", switch_code_to_char(switches[13]), switch_code_to_char(switches[14]), switch_code_to_char(switches[15]), print_switch_i+12);
            break;
        case 5:
            Print("          [16: %c], [17: %c], [18: %c]", switch_code_to_char(switches[16]), switch_code_to_char(switches[17]), switch_code_to_char(switches[18]), print_switch_i+15);
            break;
        default:
            Print("          [153: %c], [154: %c]", switch_code_to_char(switches[153]), switch_code_to_char(switches[154]), 0, print_switch_i+18);
            Print("          [155: %c], [156: %c]", switch_code_to_char(switches[155]), switch_code_to_char(switches[156]), 0, print_switch_i+20);
            break;
    }
    
}

void printSensors() {
    char sens1 = passedSensors[(sensorIdx - 1 + SENSOR_STORE) % SENSOR_STORE];
    char sens2 = passedSensors[(sensorIdx - 2 + SENSOR_STORE) % SENSOR_STORE];
    char sens3 = passedSensors[(sensorIdx - 3 + SENSOR_STORE) % SENSOR_STORE];
    char sens4 = passedSensors[(sensorIdx - 4 + SENSOR_STORE) % SENSOR_STORE];
    char sens5 = passedSensors[(sensorIdx - 5 + SENSOR_STORE) % SENSOR_STORE];
    char sens_attr1 = passedSensorAttributions[(sensorIdx - 1 + SENSOR_STORE) % SENSOR_STORE];
    char sens_attr2 = passedSensorAttributions[(sensorIdx - 2 + SENSOR_STORE) % SENSOR_STORE];
    char sens_attr3 = passedSensorAttributions[(sensorIdx - 3 + SENSOR_STORE) % SENSOR_STORE];
    char sens_attr4 = passedSensorAttributions[(sensorIdx - 4 + SENSOR_STORE) % SENSOR_STORE];
    char sens_attr5 = passedSensorAttributions[(sensorIdx - 5 + SENSOR_STORE) % SENSOR_STORE];

    Print("Recent Sensors: ", 0, 0, 0, print_sensor_i);
    Print("%s%c%d ", (long int) ( (sens_attr1 < 2) ? ((sens_attr1 == 0) ? TEXT_BLUE : TEXT_MAGENTA) : ((sens_attr1 == 2) ? TEXT_CYAN : TEXT_WHITE) ), code_to_bank(sens1), code_to_socket(sens1), print_sensor_i + 1);
    Print("%s%c%d ", (long int) ( (sens_attr2 < 2) ? ((sens_attr2 == 0) ? TEXT_BLUE : TEXT_MAGENTA) : ((sens_attr2 == 2) ? TEXT_CYAN : TEXT_WHITE) ), code_to_bank(sens2), code_to_socket(sens2), print_sensor_i + 2);
    Print("%s%c%d ", (long int) ( (sens_attr3 < 2) ? ((sens_attr3 == 0) ? TEXT_BLUE : TEXT_MAGENTA) : ((sens_attr3 == 2) ? TEXT_CYAN : TEXT_WHITE) ), code_to_bank(sens3), code_to_socket(sens3), print_sensor_i + 3);
    Print("%s%c%d ", (long int) ( (sens_attr4 < 2) ? ((sens_attr4 == 0) ? TEXT_BLUE : TEXT_MAGENTA) : ((sens_attr4 == 2) ? TEXT_CYAN : TEXT_WHITE) ), code_to_bank(sens4), code_to_socket(sens4), print_sensor_i + 4);
    Print("%s%c%d ", (long int) ( (sens_attr5 < 2) ? ((sens_attr5 == 0) ? TEXT_BLUE : TEXT_MAGENTA) : ((sens_attr5 == 2) ? TEXT_CYAN : TEXT_WHITE) ), code_to_bank(sens5), code_to_socket(sens5), print_sensor_i + 5);
}

void printPath(long int train) {
    char temp_print_buf[MESSAGE_MAX_SIZE];
    char path_print_buf[MESSAGE_MAX_SIZE];
    path_print_buf[0] = '\0';

    for (long int i = 0; i < pathLen[train]; ++i) {
        sprintf(temp_print_buf, "%s %s%s", path_print_buf, (i < pathIndex[train]) ?
                                                                TEXT_BLUE :
                                                                ((path[train][i]->TrainStopUntil[train] != -1 || path[train][i]->TrainIsStop[train]) ? 
                                                                    TEXT_RED :
                                                                    ((i == pathIndex[train]) ?
                                                                        TEXT_GREEN :
                                                                        ((i > 0 && path[train][i]->TrainStartTime[train] != path[train][i-1]->TrainEndTime[train]) ?
                                                                            TEXT_CYAN : 
                                                                            TEXT_MAGENTA))),
                 track[path[train][i]->pos].name);
        sprintf(path_print_buf, "%s", temp_print_buf);
    }

    Print("Path %ld: %s", train + 1, (long int)path_print_buf, 0, print_path_i + train);
}

void printNodes(int train) {
    Print("Train %d's path:", train + 1, 0, 0, print_log);
    for(int i = 0; i < pathLen[train]; ++i) {
        Print("start [%ld] end [%ld] command(time if stopUntil) [%d]", path[train][i]->TrainStartTime[train],path[train][i]->TrainEndTime[train], (path[train][i]->TrainStopUntil[train] != -1) ? path[train][i]->TrainStopUntil[train] : path[train][i]->TrainCommand[train], print_log);
        //Print("             dist [%d]", path[train][i]->TrainDistance[train],0,0, print_log);
        
    }
}

void trainStatusServer() {
    Print("Starting Train Status Server", 0, 0, 0, print_log);
    RegisterAs("Train Status Server");

    long int clock_server_tid;
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }


    sensorIdx = 0;

    long int tid;
    long int reply;
    char replyCh;
   
    long int train1_calibration_active = 0;
    long int train1_calibration_tid = -1;

    long int train1_control_active = 0;
    long int train1_control_tid = -1;
    long int train2_control_active = 0;
    long int train2_control_tid = -1;
   
    for (int i = 0; i < NUMTRAINS; ++i) {
        isCalculating[i] = 1;
        isActive[i] = 0;
    }

    mytrack = track;
    
    for(int i = 0; i < 144;++i){
        nodes[i].pos = i;
        
        nodes[i].prev = -1;
        nodes[i].visited = 0;

        nodes[i].postTurnoutDist = -1;

        for(int j = 0; j < NUMTRAINS;++j) {
            nodes[i].TrainDistance[j] = -1;
            nodes[i].TrainCommand[j] = COMMAND_NONE;
            nodes[i].TrainStartTime[j] = -1;
            nodes[i].TrainEndTime[j] = -1;
            nodes[i].TrainStop[j] = 0;
            nodes[i].TrainStopDist[j] = 0;
            nodes[i].TrainIsStop[j] = 0;
            nodes[i].TrainIsStartStop[j] = 0;
            nodes[i].TrainStopUntil[j] = -1;
            nodes[i].TrainStopDelay[j] = 0;
            nodes[i].TrainIsReverse[j] = 0;
            nodes[i].TrainIsAccelerating[j] = 0;
            nodes[i].TrainDir[j] = -1;
        }
    }


#ifdef IS_TRACKA
    allowedNodesLength = 66;

    allowedNodes[0] = 0; //A1
    allowedNodes[1] = 1; //A2
    allowedNodes[2] = 2; //A3
    allowedNodes[3] = 3; //A4
    allowedNodes[4] = 4; //A5
    allowedNodes[5] = 5; //A6
    allowedNodes[6] = 6; //A7
    allowedNodes[7] = 7; //A8
    allowedNodes[8] = 8; //A9
    allowedNodes[9] = 9; //A10
    //A11|A12 close to exit
    allowedNodes[10] = 12; //A13
    allowedNodes[11] = 13; //A14
    //A15|A16 close to exit

    allowedNodes[12] = 16 + 0; //B1
    allowedNodes[13] = 16 + 1; //B2
    allowedNodes[14] = 16 + 2; //B3
    allowedNodes[15] = 16 + 3; //B4
    allowedNodes[16] = 16 + 4; //B5
    allowedNodes[17] = 16 + 5; //B6
    //B7|B8 close to exit
    //B9|B10 close to exit
    //B11|B12 close to exit
    allowedNodes[18] = 16 + 12; //B13
    allowedNodes[19] = 16 + 13; //B14
    allowedNodes[20] = 16 + 14; //B15
    allowedNodes[21] = 16 + 15; //B16

    allowedNodes[22] = 32 + 0; //C1
    allowedNodes[23] = 32 + 1; //C2
    allowedNodes[24] = 32 + 2; //C3
    allowedNodes[25] = 32 + 3; //C4
    allowedNodes[26] = 32 + 4; //C5
    allowedNodes[27] = 32 + 5; //C6
    allowedNodes[28] = 32 + 6; //C7
    allowedNodes[29] = 32 + 7; //C8
    allowedNodes[30] = 32 + 8; //C9
    allowedNodes[31] = 32 + 9; //C10
    allowedNodes[32] = 32 + 10; //C11
    allowedNodes[33] = 32 + 11; //C12
    allowedNodes[34] = 32 + 12; //C13
    allowedNodes[35] = 32 + 13; //C14
    allowedNodes[36] = 32 + 14; //C15
    allowedNodes[37] = 32 + 15; //C16
    
    //D1|D2 broken turnout
    allowedNodes[38] = 48 + 2; //D3
    allowedNodes[39] = 48 + 3; //D4
    allowedNodes[40] = 48 + 4; //D5
    allowedNodes[41] = 48 + 5; //D6
    allowedNodes[42] = 48 + 6; //D7
    allowedNodes[43] = 48 + 7; //D8
    allowedNodes[44] = 48 + 8; //D9
    allowedNodes[45] = 48 + 9; //D10
    allowedNodes[46] = 48 + 10; //D11
    allowedNodes[47] = 48 + 11; //D12
    allowedNodes[48] = 48 + 12; //D13
    allowedNodes[49] = 48 + 13; //D14
    allowedNodes[50] = 48 + 14; //D15
    allowedNodes[51] = 48 + 15; //D16
    
    allowedNodes[52] = 64 + 0; //E1
    allowedNodes[53] = 64 + 1; //E2
    //E3|E4 broken turnout
    allowedNodes[54] = 64 + 4; //E5
    allowedNodes[55] = 64 + 5; //E6
    allowedNodes[56] = 64 + 6; //E7
    allowedNodes[57] = 64 + 7; //E8
    allowedNodes[58] = 64 + 8; //E9
    allowedNodes[59] = 64 + 9; //E10
    allowedNodes[60] = 64 + 10; //E11
    allowedNodes[61] = 64 + 11; //E12
    allowedNodes[62] = 64 + 12; //E13
    allowedNodes[63] = 64 + 13; //E14
    allowedNodes[64] = 64 + 14; //E15
    allowedNodes[65] = 64 + 15; //E16

#else

    //TODO: set properly for track B

    allowedNodesLength = 8;

    allowedNodes[0] = 4; //A5
    allowedNodes[1] = 5; //A6

    allowedNodes[2] = 6; //A7
    allowedNodes[3] = 7; //A8

    allowedNodes[4] = 8; //A9
    allowedNodes[5] = 9; //A10

    allowedNodes[6] = 12; //A13
    allowedNodes[7] = 13; //A14

#endif


    for (long int i = 0; i < 15; ++i) {
        train_acceleration[0][i] = TRAIN1_ACCELERATION_DEFAULT;
        train_acceleration[1][i] = TRAIN2_ACCELERATION_DEFAULT;
    }
    train_acceleration[0][8]  = TRAIN1_ACCELERATION_8;
    train_acceleration[1][8]  = TRAIN2_ACCELERATION_8;
    train_acceleration[0][10] = TRAIN1_ACCELERATION_10;
    train_acceleration[1][10] = TRAIN2_ACCELERATION_10;
    train_acceleration[0][12] = TRAIN1_ACCELERATION_12;
    train_acceleration[1][12] = TRAIN2_ACCELERATION_12;
    train_acceleration[0][14] = TRAIN1_ACCELERATION_14;
    train_acceleration[1][14] = TRAIN2_ACCELERATION_14;

    for (long int i = 0; i < 15; ++i) {
        train_deceleration[0][i] = TRAIN1_DECELERATION_DEFAULT;
        train_deceleration[1][i] = TRAIN2_DECELERATION_DEFAULT;
    }
    train_deceleration[0][8]  = TRAIN1_DECELERATION_8;
    train_deceleration[1][8]  = TRAIN2_DECELERATION_8;
    train_deceleration[0][10] = TRAIN1_DECELERATION_10;
    train_deceleration[1][10] = TRAIN2_DECELERATION_10;
    train_deceleration[0][12] = TRAIN1_DECELERATION_12;
    train_deceleration[1][12] = TRAIN2_DECELERATION_12;
    train_deceleration[0][14] = TRAIN1_DECELERATION_14;
    train_deceleration[1][14] = TRAIN2_DECELERATION_14;


    for (long int i = 0; i < 15; ++i) {
        train_speed[0][i] = TRAIN_SPEED_DEFAULT;
        train_speed[1][i] = TRAIN_SPEED_DEFAULT;
    }
    for(int i = 0; i < NUMTRAINS; ++i){
        train_speed[i][0]  = 0;
        train_current_speed[i] = 0;
    }
    
    train_speed[0][8]  = TRAIN1_SPEED8;
    train_speed[1][8]  = TRAIN2_SPEED8;
    train_speed[0][10] = TRAIN1_SPEED10;
    train_speed[1][10] = TRAIN2_SPEED10;
    train_speed[0][12] = TRAIN1_SPEED12;
    train_speed[1][12] = TRAIN2_SPEED12;
    train_speed[0][14] = TRAIN1_SPEED14;
    train_speed[1][14] = TRAIN2_SPEED14;

    trainNums[0] = TRAIN1;
    trainNums[1] = TRAIN2;

    for(int i = 0; i < NUMTRAINS; ++i){
        pathLen[i] = 0;
        pathIndex[i] = 0;
    }
    
    for (long int i = 0; i < 200; ++i) {
    	switches[i] = CURVED_CODE;
    }
    switches[153] = STRAIGHT_CODE;
    switches[154] = CURVED_CODE;
    switches[155] = STRAIGHT_CODE;
    switches[156] = CURVED_CODE;

    
    for (long int i = 0; i < 100; ++i) {
    	trainSpeeds[i] = 0;
    }
    
    for (long int i = 0; i < SENSOR_STORE; ++i) {
    	passedSensors[i] = (char)(('F' - 'A') << 4) + 15;
        passedSensorAttributions[i] = 0;
    }

    printSwitches(1);
    printSwitches(4);
    printSwitches(7);
    printSwitches(10);
    printSwitches(13);
    printSwitches(16);
    printSwitches(153);
    
    printSensors();

    printPath(0);
    
    for(;;){
        Receive(&tid, msg_buf, MESSAGE_MAX_SIZE);

        //Process request:
        
        
            
        
        if (msg_buf[0] == 'S' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'p') {
            int trainNum = msg_buf[5];
            char trainSpeed = msg_buf[6];

            trainSpeeds[trainNum] = trainSpeed;



            if (trainNum == TRAIN1) {
                train_current_speed[0] = trainSpeed;
            }else if (trainNum == TRAIN2){
                train_current_speed[1] = trainSpeed;
            }
            
            reply = 0; //can be anything
            Reply(tid, (char *)&reply, sizeof(reply));
        } 
        
        else if (msg_buf[0] == 'G' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'p') {
            int trainNum = msg_buf[5];

            replyCh = trainSpeeds[trainNum]; 
            Reply(tid, (char *)&replyCh, sizeof(replyCh));
        }

        else if (msg_buf[0] == 'S' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'w') {
            int switchNum = msg_buf[5];

            if (switches[switchNum] != msg_buf[6]) {
                switches[switchNum] = msg_buf[6];
                printSwitches(switchNum);
            }
            
            
            reply = 0; //can be anything
            Reply(tid, (char *)&reply, sizeof(reply));
        } 
        
        else if (msg_buf[0] == 'G' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'w') {
            int switchNum = msg_buf[5];
        
            replyCh = switches[switchNum]; //can be anything
            Reply(tid, (char *)&replyCh, sizeof(replyCh));
        }

        else if (msg_buf[0] == 'S' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'e'&& msg_buf[5] == 'n' && msg_buf[6] == 's') {
            
            Reply(tid, (char *)&reply, sizeof(reply));

            char data = msg_buf[7];
            if (data != passedSensors[(SENSOR_STORE + sensorIdx - 1) % SENSOR_STORE] && data != passedSensors[(SENSOR_STORE + sensorIdx - 2) % SENSOR_STORE]) {

                if (train1_calibration_active) {
                    Reply(train1_calibration_tid, &data, sizeof(char));
                    train1_calibration_active = 0;
                }

                int isTrain1Sens = 0;
                int isTrain2Sens = 0; 

                long int time = Time(clock_server_tid);

                passedSensorAttributions[sensorIdx] = 0;
                
                for (int i = 0; i < TRACK_MAX; ++i) {
                    if (track[i].type == NODE_SENSOR && track[i].num == data) {

                        if (nodes[i].TrainStartTime[0] == -1 && nodes[i].TrainStartTime[1] == -1) {
                            Print("Spurious Sensor[%s] Activated!", (long int) track[i].name, 0, 0, print_log);
                            passedSensorAttributions[sensorIdx] = 3;
                            break;
                        }

                         if (
                          //train1 start != -1,
                            nodes[i].TrainStartTime[0] != -1 &&
                          //train1 not past,
                            time < nodes[i].TrainEndTime[0] &&
                          //train1's turn
                            (
                             //(train2 start != 1,
                              nodes[i].TrainStartTime[1] == -1 ||
                             //train1 first and not past [already checked if train1 not past],
                              nodes[i].TrainStartTime[0] <= nodes[i].TrainStartTime[1] ||
                             //OR train2 past),
                              nodes[i].TrainEndTime[1] < time
                            )
                        ) {
                        
                            isTrain1Sens = 1;
                            passedSensorAttributions[sensorIdx] = 1;
                            break;
                        
                        } else if (
                          //train2 start != -1,
                            nodes[i].TrainStartTime[1] != -1 &&
                          //train2 not past,
                            time <= nodes[i].TrainEndTime[1] &&
                          //train2's turn
                            (
                             //(train1 start != 1,
                              nodes[i].TrainStartTime[0] == -1 ||
                             //train2 first and not past [already checked if train1 not past],
                              nodes[i].TrainStartTime[1] < nodes[i].TrainStartTime[0] ||
                             //OR train1 past),
                              nodes[i].TrainEndTime[0] <= time
                            )
                        ) {
                        
                            isTrain2Sens = 1;
                            passedSensorAttributions[sensorIdx] = 2;
                            break;

                        }

                        Print("Spurious Sensor[%s] Activated! [msg 2]", (long int) track[i].name, 0, 0, print_log);
                        passedSensorAttributions[sensorIdx] = 3;
                        break;
                    }
                }


                if (train1_control_active && isTrain1Sens) {
                    for (long int i = pathIndex[0] + 1; i < pathLen[0]; ++i) {
                        if (data == track[path[0][i]->pos].num && track[path[0][i]->pos].type == NODE_SENSOR) {
                            //path1Index = i - 1;
                            long int temp = i;
                            Send(train1_control_tid, (char *)&temp, sizeof(long int), &replyCh, sizeof(char));
                            break;
                        }
                    }
                }
                
                if (train2_control_active && isTrain2Sens) {
                    for (long int i = pathIndex[1] + 1; i < pathLen[1]; ++i) {
                        if (data == track[path[1][i]->pos].num && track[path[1][i]->pos].type == NODE_SENSOR) {
                            //path1Index = i - 1;
                            long int temp = i;
                            Send(train2_control_tid, (char *)&temp, sizeof(long int), &replyCh, sizeof(char));
                            break;
                        }
                    }
                }

                passedSensors[sensorIdx] = data;
                sensorIdx = (sensorIdx + 1) % SENSOR_STORE;

                printSensors();

            }
        }
        
        else if (msg_buf[0] == 'G' && msg_buf[1] == 'e' && msg_buf[2] == 't' && msg_buf[3] == 's' && msg_buf[4] == 'e'&& msg_buf[5] == 'n' && msg_buf[6] == 's') {

            if(sensorIdx < SENSOR_STORE){
                for (int i = 0; i < sensorIdx; ++i) {
                    *((long int *)(largeReply + i * sizeof(long int))) = passedSensors[i];
                }

                Reply(tid, (char *)&largeReply, sizeof(long int) * sensorIdx);
            }else{
                for (int i = 0; i < SENSOR_STORE; ++i) {
                    *((long int *)(largeReply + i * sizeof(long int))) = passedSensors[(sensorIdx + i) % SENSOR_STORE];
                }

                Reply(tid, (char *)&largeReply, sizeof(long int) * SENSOR_STORE);
            }

            
        } else if (msg_buf[0] == 'C' && msg_buf[1] == 'a' && msg_buf[2] == 'l') {
            train1_calibration_tid = tid;
            train1_calibration_active = 1;

        } else if (msg_buf[0] == 'C' && msg_buf[1] == 'o' && msg_buf[2] == 'n' && msg_buf[3] == '1' && msg_buf[4] == 'o' && msg_buf[5] == 'n') {
            Reply(tid, (char *)&reply, sizeof(reply));
            train1_control_tid = tid;
            train1_control_active = 1;

        } else if (msg_buf[0] == 'C' && msg_buf[1] == 'o' && msg_buf[2] == 'n' && msg_buf[3] == '1' && msg_buf[4] == 'o' && msg_buf[5] == 'f' && msg_buf[6] == 'f') {
            Reply(tid, (char *)&reply, sizeof(reply));
            train1_control_active = 0;

        } else if (msg_buf[0] == 'C' && msg_buf[1] == 'o' && msg_buf[2] == 'n' && msg_buf[3] == '2' && msg_buf[4] == 'o' && msg_buf[5] == 'n') {
            Reply(tid, (char *)&reply, sizeof(reply));
            train2_control_tid = tid;
            train2_control_active = 1;

        } else if (msg_buf[0] == 'C' && msg_buf[1] == 'o' && msg_buf[2] == 'n' && msg_buf[3] == '2' && msg_buf[4] == 'o' && msg_buf[5] == 'f' && msg_buf[6] == 'f') {
            Reply(tid, (char *)&reply, sizeof(reply));
            train2_control_active = 0;

        } else {
            Print("Invalid train status server command [%.6s]",(long int)msg_buf,0,0,print_log);
        }
    }
}


// Path Traversing:

void check_switch(int i, long int time) {


    if (nodes[i].TrainStartTime[0] == -1 && nodes[i].TrainStartTime[1] == -1) {
        return;
    } 

    if (
        //train1 start != -1,
        nodes[i].TrainStartTime[0] != -1 && 
        //train1 ready,
        !isCalculating[0] && isActive[0] &&
        //train2 inactive or not calculating
        (!isActive[1] || !isCalculating[1]) &&
        //train1 not past,
        time < nodes[i].TrainEndTime[0] &&
        //train1's turn
        (
            //(train2 start == -1,
            nodes[i].TrainStartTime[1] == -1 ||
            //train2 inactive [if active, it is known to be not calculating],
            !isActive[1] ||
            //train1 first and not past [already checked if train1 not past],
            nodes[i].TrainStartTime[0] <= nodes[i].TrainStartTime[1] ||
            //OR train2 past),
            nodes[i].TrainEndTime[1] + SWITCH_TIME_TOLERANCE < time
        ) &&
        //train1 not on branch (no conflict with merge)
        (
            nodes[i].TrainEndTime[0] + SWITCH_TIME_TOLERANCE < time ||
            time < nodes[i].TrainStartTime[0] - SWITCH_TIME_TOLERANCE
        ) &&
        //AND train2 not on branch (no conflict with merge)
        (
            nodes[i].TrainEndTime[1] == -1 || !isCalculating[1] || !isActive[1] ||
            nodes[i].TrainEndTime[1] + SWITCH_TIME_TOLERANCE < time ||
            time < nodes[i].TrainStartTime[1] - SWITCH_TIME_TOLERANCE
        )
    ) {

        if (switches[track[i].num] != command_to_code(nodes[i].TrainCommand[0])) {
            Print("Switching %s to %c from %c for train 1", (long int)track[i].name, (nodes[i].TrainCommand[0] == COMMAND_STRAIGHT) ? 'S' : 'C', (switches[track[i].num] == STRAIGHT_CODE) ? 'S' : 'C', print_log);
            TrainSwitch(track[i].num, (nodes[i].TrainCommand[0] == COMMAND_STRAIGHT) ? 'S' : 'C');
        }

    } else if (
        //train2 start != -1,
        nodes[i].TrainStartTime[1] != -1 && 
        //train2 ready,
        !isCalculating[1] && isActive[1] &&
        //train1 inactive or not calculating
        (!isActive[0] || !isCalculating[0]) &&
        //train2 not past,
        time <= nodes[i].TrainEndTime[1] &&
        //train2's turn
        (
            //(train1 start == -1,
            nodes[i].TrainStartTime[0] == -1 ||
            //train1 inactive [if active, it is known to be not calculating],
            !isActive[0] ||
            //train2 first and not past [already checked if train1 not past],
            nodes[i].TrainStartTime[1] < nodes[i].TrainStartTime[0] ||
            //OR train1 past),
            nodes[i].TrainEndTime[0] + SWITCH_TIME_TOLERANCE <= time
        ) &&
        //train2 not on branch (no conflict with merge)
        (
            nodes[i].TrainEndTime[1] + SWITCH_TIME_TOLERANCE <= time ||
            time <= nodes[i].TrainStartTime[1] - SWITCH_TIME_TOLERANCE
        ) &&
        //AND train1 not on branch (no conflict with merge)
        (
            nodes[i].TrainEndTime[0] == -1 || !isCalculating[0] || !isActive[0] ||
            nodes[i].TrainEndTime[0] + SWITCH_TIME_TOLERANCE <= time ||
            time <= nodes[i].TrainStartTime[0] - SWITCH_TIME_TOLERANCE
        )
    ) {

        if (switches[track[i].num] != command_to_code(nodes[i].TrainCommand[1])) {
            Print("Switching %s to %c from %c for train 2", (long int)track[i].name, (nodes[i].TrainCommand[1] == COMMAND_STRAIGHT) ? 'S' : 'C', (switches[track[i].num] == STRAIGHT_CODE) ? 'S' : 'C', print_log);
            TrainSwitch(track[i].num, (nodes[i].TrainCommand[1] == COMMAND_STRAIGHT) ? 'S' : 'C');
        }
    
    }

}

void train_switch_setter() {
    long int time = 0;

    long int clockTid; 
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }

    for (;;) {
        Delay(clockTid, 5);
        time = Time(clockTid);
        for (int i = 0; i < TRACK_MAX; ++i) {
            if (track[i].type == NODE_BRANCH) {
                check_switch(i, time);
            }
        }
    }

}

void train_control_notifier() {

    long int clockTid; 
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }

    long int train_control_tid;
    char reply;
    long int time;
    long int index;
    int curIndex;
    int train;
    int isStopUntil;


    Receive(&train_control_tid, (char *)&time, sizeof(long int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&index, sizeof(long int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&train, sizeof(int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&isStopUntil, sizeof(int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&curIndex, sizeof(int));
    Reply(train_control_tid, &reply, sizeof(char));

    if (isStopUntil) {

        while (Time(clockTid) < path[train][curIndex]->TrainStopUntil[train]) {
            Delay(clockTid, 1);
        }

        TrainSpeed(trainNums[train], train_target_speed[train]);

        if (index < pathLen[train] && track[path[train][index]->pos].type == NODE_SENSOR) {
            Delay(clockTid, (path[train][curIndex]->TrainEndTime[train] - Time(clockTid)) * (100 + TRAIN_CONTROL_DESYNC_TOLERANCE)  /  100);
        } else {     
            Delay(clockTid, path[train][curIndex]->TrainEndTime[train] - Time(clockTid));
        }

    } else {

        if (index < pathLen[train] && track[path[train][index]->pos].type == NODE_SENSOR) {
            Delay(clockTid, time * (100 + TRAIN_CONTROL_DESYNC_TOLERANCE)  /  100);
        } else {
            Delay(clockTid, time);
        }
    }

    Send(train_control_tid, (char *)&index, sizeof(long int), &reply, sizeof(char));
    Exit();
    
}

void train_command() {
    //Print("Starting train_command",0,0,0,print_log);
    //add srr for delay and command
    long int command;
    long int index;
    long int train_control_tid;
    int time;
    char reply;
    
    long int clockTid; 
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }

    Receive(&train_control_tid, (char *)&time, sizeof(long int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&index, sizeof(long int));
    Reply(train_control_tid, &reply, sizeof(char));
    Receive(&train_control_tid, (char *)&command, sizeof(long int));
    Reply(train_control_tid, &reply, sizeof(char));

    Print("Got params time:[%ld], index:[%ld], command:[%ld]",time,index,command,print_log);

    Delay(clockTid, time);
    if(command == COMMAND_STOP){
        Print("Stopped train [%ld] after delay[%ld]", trainNums[index],time,0,print_log);
        TrainSpeed(trainNums[index], 0);
    }else if(command == COMMAND_REVERSE){
        Print("Starting train [%ld] reverse (then set to [%ld])", trainNums[index],train_target_speed[index],0,print_log);
        //TrainSpeed(index,0);
        //Delay(clockTid,REVERSE_DELAY); 
        //TrainReverse(index);
        pure_train_reverse(trainNums[index]);
        if(time != -1)   TrainSpeed(trainNums[index], train_target_speed[index]); 
    }else if(command == COMMAND_CURVED){
        Print("Flipping switch [%ld] to curved", index,0,0,print_log);
        TrainSwitch(index,'C');
    }else if(command == COMMAND_STRAIGHT){
        Print("Flipping switch [%ld] to straight", index,0,0,print_log);
        TrainSwitch(index,'S');
    }
    
    Exit();
}

void train_control() {
    
    char tr;
    int train;
    char isAuto;
    char reply;
    long int msg;
    long int tid;

    long int time;
    long int index;
    long int command;

    long int train_status_server_tid;
    long int clock_server_tid;
    long int trainRT_server_tid;
    while ((train_status_server_tid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }
    while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }

    Receive(&tid, &tr, sizeof(char));
    Reply(tid, &reply, sizeof(char));
    Receive(&tid, &isAuto, sizeof(char));
    Reply(tid, &reply, sizeof(char));
    train = tr;

    Print("Created a Train %d Control Task", train + 1, 0, 0, print_log);

    if(train < 0 || train >= NUMTRAINS){
        Print("Invalid train passed [%ld]",train,0,0,print_log);
    }

    Send(train_status_server_tid, (train == 0) ? "Con1on" : "Con2on", sizeof("Con#on"), &reply, sizeof(char));

    for (;;) {
        //At this point: isActive = 0;

        if (isAuto) {
            Send(MyParentTid(), (char *)&msg, sizeof(long int), &reply, sizeof(reply));
        }


        isCalculating[train] = 0;
        isActive[train] = 1;

        if (pathLen[train] > 0 && path[train][0]->TrainStopUntil[train] == -1) {
            Print("Setting Train %d to speed %d", train + 1, train_target_speed[train], 0, print_log);
            TrainSpeed(trainNums[train], train_target_speed[train]);
        }
        

        for (pathIndex[train] = 0; pathIndex[train] < pathLen[train]; ++pathIndex[train]) {

            printPath(train);
            time = Time(clock_server_tid);

        if (pathIndex[train] > 0 && path[train][pathIndex[train]]->TrainStartTime[train] == path[train][pathIndex[train] - 1]->TrainStartTime[train] && track[path[train][pathIndex[train]]->pos].num != passedSensors[(SENSOR_STORE + sensorIdx - 1) % SENSOR_STORE] && track[path[train][pathIndex[train]]->pos].type == NODE_SENSOR) {
            Print("Train %d Predicted - Actual: \033[37m[ Time: %ld Ticks | ~Dist: %ld um ]", train + 1, path[train][pathIndex[train]]->TrainStartTime[train] - time, (path[train][pathIndex[train]]->TrainStartTime[train] - time) * train_speed[train][train_current_speed[train]], print_train_diff);
        }
        
        if (path[train][pathIndex[train]]->TrainIsStartStop[train]) {
            Print("Starting stop for train %d at index [%ld], with delay [%ld]",train + 1, index,path[train][pathIndex[train]]->TrainStopDelay[train], print_log);
            long int command_tid = Create(SERVER_PRIORITY, train_command);
            time = path[train][pathIndex[train]]->TrainStopDelay[train];
            index = train;
            command = COMMAND_STOP;
            //Print("Sending params time:[%ld], index:[%ld], command:[%ld]",time,index,command,print_log);
            Send(command_tid,(char *)&time,sizeof(long int),(char *)&reply, sizeof(reply));
            Send(command_tid,(char *)&index,sizeof(long int),(char *)&reply, sizeof(reply));
            Send(command_tid,(char *)&command,sizeof(long int),(char *)&reply, sizeof(reply));
        }
        if(path[train][pathIndex[train]]->TrainIsReverse[train]){
            Print("Reverse for train %d at %s",train + 1, (long int)track[path[train][pathIndex[train]]->pos].name, 0, print_log);
            long int command_tid = Create(SERVER_PRIORITY, train_command);
            if(path[train][pathIndex[train]]->TrainStopUntil[train] != -1 || pathIndex[train] == pathLen[train] - 1){
                time = -1;
            }else{
                time = 0;
            }
            index = train;
            command = COMMAND_REVERSE;
            //Print("Sending params time:[%ld], index:[%ld], command:[%ld]",time,index,command,print_log);
            Send(command_tid,(char *)&time,sizeof(long int),(char *)&reply, sizeof(reply));
            Send(command_tid,(char *)&index,sizeof(long int),(char *)&reply, sizeof(reply));
            Send(command_tid,(char *)&command,sizeof(long int),(char *)&reply, sizeof(reply));
            
        }

            if (pathIndex[train] + 1 >= pathLen[train]) {
                break;
            }

            long int notifier_tid = Create(3, train_control_notifier);
            time = path[train][pathIndex[train]]->TrainEndTime[train] - path[train][pathIndex[train]]->TrainStartTime[train];
            //Print("Curr node = [%s], waiting for [%ld]", (long int)track[path[train][pathIndex[train]]->pos].name, time,0, print_log);
            if(path[train][pathIndex[train]]->TrainIsAccelerating[train]){
                
                for(index = pathIndex[train];index<pathLen[train] && path[train][index]->TrainEndTime[train] == path[train][pathIndex[train]]->TrainEndTime[train];++index){
                }
                //index--;
            }else{
                index = pathIndex[train]+1;
            }

            int isStopUntil = path[train][pathIndex[train]]->TrainStopUntil[train] != -1;
            int curIndex = pathIndex[train];
            Print("Sending to %s notifier with time [%ld] for train %d", (long int)track[path[train][index]->pos].name, isStopUntil ? -42069 : time, train + 1, print_log);
            Send(notifier_tid, (char *)&time, sizeof(long int), &reply, sizeof(char));
            Send(notifier_tid, (char *)&index, sizeof(long int), &reply, sizeof(char));
            Send(notifier_tid, (char *)&train, sizeof(int), &reply, sizeof(char));
            Send(notifier_tid, (char *)&isStopUntil, sizeof(int), &reply, sizeof(char));
            Send(notifier_tid, (char *)&curIndex, sizeof(int), &reply, sizeof(char));

            for (;;) {
                Receive(&tid, (char *)&msg, sizeof(long int));
                Reply(tid, &reply, sizeof(char));
                //Print("Msg: [%ld]",msg,0,0,print_log);
                if(msg > pathIndex[train]+1){
                    for(int i = pathIndex[train] +1;i<msg;++i){

                        if (path[train][i]->TrainIsStartStop[train]) {
                            Print("Train %d Stop (catch up) from %s", train + 1, (long int) track[path[train][i]->pos].name, 0, print_log);
                            TrainSpeed(trainNums[train],0);
                        }
                        if(path[train][i]->TrainIsReverse[train]){
                            Print(" !!!!  Train %d Reverse (catch up) from %s", train + 1, (long int) track[path[train][i]->pos].name, 0, print_log);
                            pure_train_reverse(trainNums[train]);
                            if(i != pathLen[train]-1) TrainSpeed(trainNums[train],train_target_speed[train]);
                        }
                    }
                    pathIndex[train] = msg-1;
                    break;
                }
                if (msg > pathIndex[train]) {
                    
                    break;
                }

                if(tid == notifier_tid){

                }
                Print("Loop (pathIndex[train] is [%ld], msg was %ld)",pathIndex[train],msg,0,print_log);
            }
            //Print("Out of for loop",0,0,0,print_log);
            if(pathIndex[train]+1 < pathLen[train] && !path[train][pathIndex[train]+1]->TrainIsAccelerating[train]){
                Print("Remeasuring at [%s]", (long int)track[path[train][index]->pos].name, 0, 0, print_log);
                isCalculating[train] = 1;
                newSetup(pathIndex[train]+1,train,train_target_speed[train],train_speed[train][train_current_speed[train]]);
                isCalculating[train] = 0;
            }

        }

        Print("Train %d Reached end of path!", train + 1, 0, 0, print_log);
        isActive[train] = 0;

        if (!isAuto) {
            break;
        }
    }

    Exit();
}

int invalidTrainDest(int train, int start, int dest) {
    if (dest == start || dest == track[start].edge[DIR_AHEAD].reverse->src->idx || dest == track[start].reverse->idx) {
        return 1;
    }

    int otherTrain = 1 - train;

    for (int i = 0; i < pathLen[otherTrain]; ++i) {
        if (track[path[otherTrain][i]->pos].type == NODE_BRANCH) {
           
            if (dest == path[otherTrain][i]->pos || dest == track[path[otherTrain][i]->pos].edge[DIR_STRAIGHT].reverse->src->idx || dest == track[path[otherTrain][i]->pos].edge[DIR_CURVED].reverse->src->idx) {
                return 1;
            }   

        } else if (track[path[otherTrain][i]->pos].type != NODE_ENTER && track[path[otherTrain][i]->pos].type != NODE_EXIT) {
           
            if (dest == path[otherTrain][i]->pos || dest == track[path[otherTrain][i]->pos].edge[DIR_AHEAD].reverse->src->idx) {
                return 1;
            }       

        }
    }

    return 0;
}


void auto_dest() {

    char reply;
    char msg;
    long int tid1, tid2;

    int pos1[NUMTRAINS];
    int pos2[NUMTRAINS];

    long int clock_server_tid;
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }


    Receive(&tid1, (char *)&pos1[0], sizeof(int));
    Reply(tid1, &reply, sizeof(char));
    Receive(&tid1, (char *)&pos1[1], sizeof(int));
    Reply(tid1, &reply, sizeof(char));


    tid1 = Create(SERVER_PRIORITY, train_control);
    tid2 = Create(SERVER_PRIORITY, train_control);

    //train = 0/1:
    msg = 0;
    Send(tid1, &msg, 1, &reply, 1);
    msg = 1;
    Send(tid2, &msg, 1, &reply, 1);

    //isAuto = 1:
    Send(tid1, &msg, 1, &reply, 1);
    Send(tid2, &msg, 1, &reply, 1);

    for (;;) {
        Receive(&tid1, &msg, sizeof(char));
        Receive(&tid2, &msg, sizeof(char));
        
        Delay(clock_server_tid, 500);
        
        Print("Generating next paths!", 0, 0, 0, print_log);

        long int attempts = -1;
        do {
            do {
                //Randomly generate dests (and paths)
                do {
                    pos2[0] = allowedNodes[(*CLO) % allowedNodesLength];
                    Yield();
                    ++attempts;
                } while (pos2[0] == pos1[0] || pos2[0] == track[pos1[0]].reverse->idx);

                dijkstras(pos1[0], 0, pos2[0], 0, 0);

                do {
                    pos2[1] = allowedNodes[(*CLO) % allowedNodesLength];
                    Yield();
                    ++attempts;
                } while (invalidTrainDest(1, pos1[1], pos2[1]));
                
                dijkstras(pos1[1], 0, pos2[1], 0, 1);

            } while (invalidTrainDest(0, pos1[0], pos2[0]));

            Print("Decided start and end of paths after %ld attempts", attempts, 0, 0, print_log);
        } while (recalculate(8) == -1);


        pos1[0] = pos2[0];
        pos1[1] = pos2[1];
        
        Reply(tid1, &msg, sizeof(char));
        Reply(tid2, &msg, sizeof(char));
        
        Reply(tid1, &msg, sizeof(char));
        Reply(tid2, &msg, sizeof(char));
        
    }


}



//Path Finding:

// return time & dist_mm for full accelerate or full stop from velocity to speed/0
long int accel_val(int *dist_mm, int speed, long int velocity, int is_stop, int train) {
    long int v1_nm;
    long int v2_nm;
    long int a_nm;

    if (velocity > train_speed[train][speed] || velocity < 0) {
        Print("Passed invalid velocity[%ld] to accel_val()!", velocity, 0, 0, print_log);
        return -1;
    }

    if (is_stop) {
        v1_nm = velocity * 1000;
        v2_nm = 0;
        a_nm = train_deceleration[train][speed];
    } else {
        v1_nm = velocity * 1000;
        v2_nm = train_speed[train][speed] * 1000;
        a_nm = train_acceleration[train][speed];
    }

    *dist_mm = (v2_nm*v2_nm - v1_nm*v1_nm) / (2 * a_nm * 1000 * 1000);
    return (v2_nm - v1_nm) / a_nm;
    
}

// Return total time and cmd_delay of 0->speed->0 movement for length dist
long int move_dist(int dist_mm, long int *cmd_delay, int speed, int train) {
    int dist_accel = 0;
    int dist_stop = 0;
    accel_val(&dist_accel, speed, 0, 0, train);
    accel_val(&dist_stop, speed, train_speed[train][speed], 1,train);
    if (dist_mm > dist_accel + dist_stop + 3) {
        Print("Passed dist[%d] > dist_accel[%d] + dist_stop[%d] to move_dist()!", dist_mm, dist_accel, dist_stop, print_log);
        *cmd_delay = 0;
        return 0;
    }
    long int d_nm = dist_mm * 1000 * 1000;
    //d_nm = (9171931 * d_nm / 10000000) - 45551620;
    //d_nm = (3870527 * d_nm / 10000000) + (4980901 * d_nm * d_nm / 100000000000000000) + 34043400;
    d_nm = d_nm * 8 / 10;
    if (d_nm < 0) d_nm = 0;

    long int a1_nm = train_acceleration[train][speed];
    long int a2_nm = -1 * train_deceleration[train][speed];

    *cmd_delay = sqrt( (2 * a2_nm * d_nm) / (a1_nm*a2_nm + a1_nm*a1_nm) ) - 5;
    return *cmd_delay + ( (a1_nm * *cmd_delay) / a2_nm );
}

void recalcPathDist(int train){
    int dist = 0;
    for(int i = 0 ; i < pathLen[train];++i){
        path[train][i]->TrainDistance[train] = dist;
        Print("Set path train [%ld]'s distance to [%ld]",i,path[train][i]->TrainDistance[train],0,print_debug);
        if(path[train][i]->TrainCommand[train] == COMMAND_REVERSE){
            dist += 0;
        }else if(path[train][i]->TrainCommand[train] == COMMAND_CURVED){
            dist += track[path[train][i]->pos].edge[DIR_CURVED].dist;
        }else if(path[train][i]->TrainCommand[train] == COMMAND_STRAIGHT){
            dist += track[path[train][i]->pos].edge[DIR_STRAIGHT].dist;
        }else{
            dist += track[path[train][i]->pos].edge[DIR_AHEAD].dist;
        }
    }
}

void multiTrackFix(int train){
    int branchPos = 0;
    for(; path[train][branchPos]->TrainDistance[train] < SWITCH_DIST_TOLERANCE; ++branchPos){
        if(track[path[train][branchPos]->pos].type == NODE_BRANCH){ //found branch before enogh disance has passed


            Print("Need to immediately reverse to get space for branch",0,0,0,print_log);
                
            reverseInsert[0] = track[path[train][0]->pos].reverse->idx;
            int next = -1;
            int dist = 0;
            int i = 1;
            for(;dist < SWITCH_DIST_TOLERANCE;++i){
                Print("i = [%ld], dist = [%ld] ", i, dist,0,print_debug);
                if(track[reverseInsert[i-1]].type != NODE_BRANCH && track[reverseInsert[i-1]].type != NODE_EXIT){
                    next = track[reverseInsert[i-1]].edge[DIR_AHEAD].dest->idx;
                    dist += track[reverseInsert[i-1]].edge[DIR_AHEAD].dist;
                }else if(track[reverseInsert[i-1]].type == NODE_EXIT){
                    break;
                }else{ //arbitrarily going straight
                    next = track[reverseInsert[i-1]].edge[DIR_STRAIGHT].dest->idx;
                    dist += track[reverseInsert[i-1]].edge[DIR_STRAIGHT].dist;
                }
                    reverseInsert[i] = next;
                }
                for(int j = 0; j < i ; j++){
                    Print("Moving back to node [%ld], i=%ld", reverseInsert[j], i, 0, print_debug);
                    Print("   aka [%s] - 1", (long int) track[reverseInsert[j]].name, 0, 0, print_debug);
                }
                Print("Starting other way (ew ugly we hate this)",0,0,0,print_debug);
                int firstBatch = i;
                    
                //FIGURED OUT THIS PART                    
                for(int idx = firstBatch -1; idx > 0; --idx){
                    reverseInsert[i] = track[reverseInsert[idx]].reverse->idx;
                    i++; 
                }
                
                //shifting over    
                Print("shifting over",0,0,0,print_debug);
                for(int pos = pathLen[train]-1; pos >= 0;--pos ){
                    path[train][pos+i] = path[train][pos];
                }
                //adding nodes to start
                Print("adding nodes to start",0,0,0,print_debug);
                for(int pos = 0; pos < i;++pos){
                    path[train][pos] = &nodes[reverseInsert[pos]];
                }

                path[train][firstBatch-1]->TrainCommand[train] = COMMAND_REVERSE;

                //setting branch directions
                Print("setting branch directions",0,0,0,print_debug);
                for(int pos = 0; pos < i-1; ++pos){
                    if(track[path[train][pos]->pos].type == NODE_BRANCH){
                        if(track[path[train][pos]->pos].edge[DIR_STRAIGHT].dest->idx == path[train][pos+1]->pos ){
                            path[train][pos]->TrainCommand[train] = COMMAND_STRAIGHT;
                        }else if(track[path[train][pos]->pos].edge[DIR_CURVED].dest->idx == path[train][pos+1]->pos ){
                            path[train][pos]->TrainCommand[train] = COMMAND_CURVED;
                        }else{
                            Print("Branch [%ld] not leading to next: [%ld]",path[train][pos]->pos, path[train][pos+1]->pos,0,print_debug);
                        }
                    }
                }
                pathLen[train] += firstBatch*2-1;
                //recalclating path distance 
                recalcPathDist(train);

                Print("!!!!!!!!!!!!!!!!!!Flipping train [%ld] direction",train + 1,0,0,print_log);
                pure_train_reverse(trainNums[train]);
                return;
        }
    }
}

int extend(int train){
    long int lowest = INT32_MAX;
    int loc = -1;
    int postTurnDist = -1;
    int curved = COMMAND_NONE;
    for(int i = dijStart;i < dijEnd; ++i){
        struct track_node n = track[dijs[i]->pos];
        int dis = dijs[i]->TrainDistance[train];
        
        if(!nodes[n.reverse->idx].visited && nodes[n.idx].postTurnoutDist > COLLISION_DIST_TOLERANCE){ //Michael modify this later
            lowest = dis;
            loc = i;
            curved = COMMAND_REVERSE;
            break;
        }
        if(n.type == NODE_EXIT){
            continue;
        }
        if(n.type != NODE_BRANCH){
            if(nodes[n.edge->dest->idx].visited){
                continue;
            }
            if(n.edge->dist + dis < lowest){
                lowest = n.edge->dist + dis;
                loc = i;
                curved = COMMAND_NONE;
                postTurnDist = n.edge->dist + nodes[n.idx].postTurnoutDist;
            }
        }else{
            if(!nodes[n.edge[DIR_STRAIGHT].dest->idx].visited && n.edge[DIR_STRAIGHT].dist + dis < lowest){
                lowest = n.edge[DIR_STRAIGHT].dist + dis;
                loc = i;
                curved = COMMAND_STRAIGHT;
                postTurnDist = n.edge[DIR_STRAIGHT].dist + nodes[n.idx].postTurnoutDist;
            }
            if(!nodes[n.edge[DIR_CURVED].dest->idx].visited && n.edge[DIR_CURVED].dist + dis < lowest){
                lowest = n.edge[DIR_CURVED].dist + dis;
                loc = i;
                curved = COMMAND_CURVED;
                postTurnDist = n.edge[DIR_CURVED].dist + nodes[n.idx].postTurnoutDist;
            }

        }
    }

    if(loc == -1){
        return -1;
    }

    
    struct dijNode *no = dijs[loc]; 
    
    struct track_node tn = track[no->pos];
    if (curved == COMMAND_REVERSE){
        dijs[dijEnd] = &nodes[tn.reverse->idx];
        dijs[dijEnd]->pos = tn.reverse->idx;
    }else{
        dijs[dijEnd] = &nodes[tn.edge[curved].dest->idx];
        dijs[dijEnd]->pos = tn.edge[curved].dest->idx;
    }
    
    if(track[dijs[dijEnd]->pos].type == NODE_MERGE) postTurnDist = 0;

    dijs[dijEnd]->TrainDistance[train] = lowest;
    dijs[dijEnd]->prev = loc;
    dijs[dijEnd]->visited = 1;
    
    dijs[dijEnd]->TrainCommand[train] = curved;
    
    
    dijs[dijEnd]->postTurnoutDist = postTurnDist;

    dijEnd++;
    return 0;
}

void dijkstras(long int start,long int startOffset, long int dest, long int destOffset, int train){
    Print("Going from [%ld] to [%ld]", start,dest,0,print_log);
    for(int i = 0; i < 144; ++i) {
        nodes[i].pos = i;
        nodes[i].TrainDistance[train] = -1;
        nodes[i].prev = -1;
        nodes[i].visited = 0;

        nodes[i].postTurnoutDist = -1;

        nodes[i].TrainCommand[train] = COMMAND_NONE;
        nodes[i].TrainStartTime[train] = -1;
        nodes[i].TrainEndTime[train] = -1;
        nodes[i].TrainStop[train] = 0;
        nodes[i].TrainStopDist[train] = 0;
        nodes[i].TrainIsStop[train] = 0;
        nodes[i].TrainIsStartStop[train] = 0;
        nodes[i].TrainStopUntil[train] = -1;
        nodes[i].TrainStopDelay[train] = 0;
        nodes[i].TrainIsReverse[train] = 0;
        nodes[i].TrainIsAccelerating[train] = 0;
        nodes[i].TrainDir[train] = -1;
        
    }

    dijStart = 0;
    dijs[dijStart] = &nodes[start];
    dijs[dijStart]->visited = 1;
    dijs[dijStart]->TrainDistance[train] = -startOffset;
    dijs[dijStart]->prev = -1;
    dijs[dijStart]->pos = start;
    dijs[dijStart]->postTurnoutDist = (track[dijs[dijStart]->pos].type == NODE_MERGE) ? 0 : 9999;
    dijEnd = 1;
    while(dijs[dijEnd-1]->pos != dest){
        int val = extend(train);
        if(val == -1){
            Print("Path is unreachable (somehow)",0,0,0,print_log);
            return;
        }
        for(int i = 0; i < 144; ++i){
            nodes[i].TrainStartTime[train] = -1;
            nodes[i].TrainEndTime[train] = -1;
        }
        if(dijs[dijEnd-1]->TrainCommand[train] == COMMAND_REVERSE){
            dijs[dijEnd-1]->TrainIsReverse[train] = 1;
        }
        if(dijs[dijEnd-1]->TrainCommand[train] == COMMAND_STOP){
            dijs[dijEnd-1]->TrainIsStop[train] = 1;
        }
        
        
    }
    dijs[dijEnd-1]->TrainDistance[train] += destOffset;
    struct dijNode *temp = dijs[dijEnd-1];

    long int myPathLen = 0;
    pathIndex[train] = 0;
    
    for(;;){
        path[train][myPathLen] = temp; 
        ++myPathLen;
        if(temp->prev == -1){
            break;
        }
        temp = &nodes[dijs[temp->prev]->pos];

    }

    if(track[path[train][myPathLen -1]->pos].reverse->idx == path[train][myPathLen-2]->pos){
        myPathLen--;
        pure_train_reverse(trainNums[train]);
        TrainSpeed(trainNums[train],train_current_speed[train]);
        Print("Reversed train [%ld] then set to [%ld]", trainNums[train], train_current_speed[train],0,print_log);
        path[train][myPathLen-1]->TrainCommand[train] = COMMAND_NONE;
        path[train][myPathLen-1]->TrainIsReverse[train] = 0;
    }  

    for(int i = 0;i < myPathLen/2; ++i){
        struct dijNode *temp = path[train][i];
        path[train][i] = path[train][myPathLen-1-i];
        path[train][myPathLen-1-i] = temp;
    }
    for(int i = 0; i < myPathLen - 1; ++i){
        //Michael change this?
        path[train][i]->TrainCommand[train] = path[train][i+1]->TrainCommand[train];
    }

    path[train][myPathLen-1]->TrainCommand[train] = COMMAND_STOP;
    pathLen[train] = myPathLen;

    multiTrackFix(train);

    printPath(train);
    
} 



void traverse_path(char speed, int train, char isAuto) {
    char reply;
    char tr = train;
    
    //Print("Calculating Stopping", 0, 0, 0, print_log);
    Print("Using new setup", 0, 0, 0, print_log);
    newSetup(0,train,speed,0);
    //setupTrack(1);
    Print("Creating Train %d Control Task", train + 1, 0, 0, print_log);
    long int tid = Create(SERVER_PRIORITY, train_control);
    Send(tid,&tr,1,&reply,1);
    Send(tid,&isAuto,1,&reply,1);
    
}


/*
PLEASE DO NOT USE THIS IN ANY CAPACITY UNTIL YOU FIGURE OUT WHAT IS GOING TO HAPPEN WITH THE FIRST NODE
WE'RE ABOUT TO HAVE A LOT OF BAD MEMORY ACCESS
let's run it
*/
void newSetup(int index, int train, int full_speed, int cur_velocity){ 
    Print("resetting values",0,0,0,print_debug);
    for(int i = 0; i < 144; ++i) {
        nodes[i].pos = i;
        //nodes[i].TrainDistance[train] = -1;
        nodes[i].prev = -1;
        nodes[i].visited = 0;

        nodes[i].postTurnoutDist = -1;

        nodes[i].TrainCommand[train] = COMMAND_NONE;
        nodes[i].TrainStartTime[train] = -1;
        nodes[i].TrainEndTime[train] = -1;
        nodes[i].TrainStop[train] = 0;
        nodes[i].TrainStopDist[train] = 0;
        nodes[i].TrainIsStop[train] = 0;
        nodes[i].TrainIsStartStop[train] = 0;
        //nodes[i].TrainStopUntil[train] = -1;
        nodes[i].TrainStopDelay[train] = 0;
        nodes[i].TrainIsReverse[train] = 0;
        nodes[i].TrainIsAccelerating[train] = 0;
        nodes[i].TrainDir[train] = -1;
        
    }


    Print("newSetup time, cur_velocity = [%ld]",cur_velocity,0,0,print_debug);
    int v = cur_velocity;
    int curNum;
    //int curOffset;
    int nextStop = -1;
    int full_velocity; 
    int len;
    long int clock_server_tid;
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }

    len = pathLen[train];
    Print("Setting up Commands for train [%ld]",train + 1,0,0,print_debug);
    for(int i =0; i < len-1; ++i){
        if(track[path[train][i]->pos].reverse->idx == path[train][i+1]->pos){
            path[train][i]->TrainCommand[train] = COMMAND_REVERSE;
            path[train][i+1]->TrainIsReverse[train] = 1;
        }
        else if(path[train][i]->TrainStopUntil[train] != -1){
            path[train][i]->TrainCommand[train] = COMMAND_STOP;
            path[train][i+1]->TrainIsStop[train] = 1;
        }
        else if(track[path[train][i]->pos].type == NODE_BRANCH){
            if(track[path[train][i]->pos].edge[DIR_STRAIGHT].dest->idx == path[train][i+1]->pos){
                path[train][i]->TrainCommand[train] = COMMAND_STRAIGHT;
            }else if(track[path[train][i]->pos].edge[DIR_CURVED].dest->idx == path[train][i+1]->pos){
                path[train][i]->TrainCommand[train] = COMMAND_CURVED;
            }else{
                Print("Neither option of branch node leads to next node",0,0,0,print_log);
            }
        }
    }

    path[train][len-1]->TrainCommand[train] = COMMAND_STOP;
    
    train_target_speed[train] = full_speed;
    if(index >= len) return;
    path[train][index]->TrainStartTime[train] = Time(clock_server_tid);
    //Print(" path[train][index]->TrainStartTime[train]: [%ld]", path[train][index]->TrainStartTime[train],0,0,print_debug);
    full_velocity = train_speed[train][full_speed];

    curNum = index;
    for(;curNum < len-1;){ //?
        //Print("curNum is [%ld]",curNum,0,0,print_debug);
        if(curNum != index) path[train][curNum]->TrainStartTime[train] = path[train][curNum-1]->TrainEndTime[train];
        if(path[train][curNum]->TrainCommand[train] == COMMAND_REVERSE){
            //Print("Doing reverse skip from [%ld] to [%ld]", curNum, curNum+1,0,print_debug);

            if(curNum != index) path[train][curNum]->TrainStartTime[train] = path[train][curNum-1]->TrainEndTime[train]; 
            path[train][curNum]->TrainEndTime[train] = path[train][curNum]->TrainStartTime[train] +1;
            path[train][curNum+1]->TrainStartTime[train] = path[train][curNum]->TrainEndTime[train];
            
            curNum++;
            v = 0;
            continue;
        }
        //Print("CurNum: [%ld], Len: [%ld]",curNum, len,0,print_debug);
        for(int j = curNum+1; j <= len; j++){
            if(j == len){
                Print("No stop found, something is wrong",0,0,0,print_debug);
                return;
            }
            if(path[train][j]->TrainCommand[train] == COMMAND_STOP || path[train][j]->TrainCommand[train] == COMMAND_REVERSE){ //Michael look into this
                nextStop = j;
                //Print("nextStop = [%ld], distance = [%ld]", nextStop,path[train][nextStop]->TrainDistance[train],0,print_debug);
                break;
            }
        }
             
        int accel_dist = 0;
        int accel_time = accel_val(&accel_dist,full_speed,v,0, train); //time/dist to full speed
        int decel_dist = 0;
        int decel_time = accel_val(&decel_dist,full_speed,full_velocity,1, train);
        //Print("Accel dist [%ld], time [%ld]", accel_dist, accel_time,0,print_debug);
        if(v == 0){
            //Print("v == 0",0,0,0,print_debug);
            
            if(path[train][nextStop]->TrainDistance[train] - path[train][curNum]->TrainDistance[train] < accel_dist + decel_dist){ //not enough space to accelerate and decelerate
                Print("not enough space, using movedist [%ld]",path[train][nextStop]->TrainDistance[train] - path[train][curNum]->TrainDistance[train],0,0,print_debug);
                long int stop_time;
                int tot_time = move_dist(path[train][nextStop]->TrainDistance[train] - path[train][curNum]->TrainDistance[train],&stop_time, full_speed, train);
                if(tot_time < 0) Print("mov dist gave negative time???: [%ld]",tot_time,0,0,print_debug);
                int k = curNum;
                if(k != index) path[train][k]->TrainStartTime[train] = path[train][k-1]->TrainEndTime[train]; 
                for(k = curNum;k < nextStop;++k){
                    if(path[train][k]->TrainStopUntil[train] != -1){
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStopUntil[train] + tot_time;
                        path[train][k+1]->TrainStartTime[train] = path[train][k]->TrainStartTime[train];//entire movedist is 1 block 
                    }else{
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStartTime[train] + tot_time;
                        path[train][k+1]->TrainStartTime[train] = path[train][k]->TrainStartTime[train]; //entire movedist is 1 block 
                    }
                    path[train][k]->TrainIsAccelerating[train] = 1;
                }
                path[train][curNum]->TrainIsStartStop[train] = 1;
                path[train][curNum]->TrainStopDelay[train] = stop_time;
                
                Print("Done movedist, k = [%ld], curNum = [%ld]",k,curNum,0,print_debug);
                path[train][k]->TrainStartTime[train] = path[train][curNum]->TrainEndTime[train];
                curNum = k;
                v = 0;
            }else{ //enough space, go to full speed
                if(curNum != index) path[train][curNum]->TrainStartTime[train] = path[train][curNum-1]->TrainEndTime[train];
                //Print("v == 0, going to full speed, accel dist: [%ld], decel dist: [%ld] ",accel_dist,decel_dist,0,print_debug);
                int k = curNum;
                //int temp_time = -1;
                int mydist;
                
                mydist = path[train][k+1]->TrainDistance[train] - path[train][k]->TrainDistance[train];  //-1 on both? Michael
                if(mydist < 0) Print("??????????mydist negative???: [%ld] [%ld] - [%ld]",mydist,path[train][k+1]->TrainDistance[train],path[train][k]->TrainDistance[train],print_debug);
                
                if(accel_time < 0) Print("accel val gave negative time???: [%ld]",accel_time,0,0,print_debug);
                
                
                for(;accel_dist > mydist;){
                    
                    //temp_time = accel_val(&dist,full_speed,0,0);
                    
                    path[train][k]->TrainStartTime[train] = path[train][curNum]->TrainStartTime[train];

                    if(path[train][k]->TrainStopUntil[train] != -1){
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStopUntil[train] + accel_time;
                    }else{
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStartTime[train] + accel_time;
                    }

                    path[train][k]->TrainIsAccelerating[train] = 1;
//                    Print("Node [%ld] start [%ld] end [%ld]",k,path[train][k]->TrainStartTime[train],path[train][k]->TrainEndTime[train], print_debug);
                    
                    
                    accel_dist -= mydist;
                    ++k;
                    mydist = path[train][k+1]->TrainDistance[train] - path[train][k]->TrainDistance[train]; 
                }

                v = full_velocity;
                //Print("One extra node, just in case k:[%ld] mydist: [%ld], accel_dist: [%ld]",k,mydist,accel_dist,print_debug);
                
                if (k != curNum) path[train][k]->TrainStartTime[train] = path[train][curNum]->TrainEndTime[train];
                if(path[train][k+1]->TrainDistance[train] - path[train][k]->TrainDistance[train] < 0) Print("Distance not setup well: [%ld] > [%ld]",path[train][k+1]->TrainDistance[train], path[train][k]->TrainDistance[train],0,print_debug);
                
                if(path[train][k]->TrainStopUntil[train] != -1){
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStopUntil[train] + accel_time + (path[train][k+1]->TrainDistance[train] - path[train][k]->TrainDistance[train] - accel_dist)*1000/v;
                    }else{
                        path[train][k]->TrainEndTime[train] = path[train][k]->TrainStartTime[train] + accel_time + (path[train][k+1]->TrainDistance[train] - path[train][k]->TrainDistance[train] - accel_dist)*1000/v;
                }
                path[train][k]->TrainIsAccelerating[train] = 1;
                    //path[train][k+1]->TrainStartTime[train] = path[train][k]->TrainEndTime[train];
//                    Print("Node [%ld] start [%ld] end [%ld]",k,path[train][k]->TrainStartTime[train],path[train][k]->TrainEndTime[train], print_debug);
                

                for(int i = curNum; i < k; ++i){
                    path[train][i]->TrainEndTime[train] = path[train][k]->TrainEndTime[train];
                }


                ++k;
                curNum = k;
                //Print("Finished going to full speed: [%ld], new node = [%ld]",v,curNum,0, print_debug);
            }
        }else{ //v = full speed
            //Print("v == full speed",0,0,0,print_debug);
            //find stopping node
            int stoppingNode = -1;
            int distLeft = decel_dist;
             
            for(int i = nextStop; i>0; --i){
                if(distLeft < path[train][i]->TrainDistance[train] - path[train][i-1]->TrainDistance[train]){
                    
                    path[train][i-1]->TrainIsStartStop[train] = 1;
                    path[train][i-1]->TrainStopDist[train] = path[train][i]->TrainDistance[train] - distLeft - path[train][i-1]->TrainDistance[train];
                    //Print("Setup stop for train %ld at %s with distance %ld",train + 1,(long int)track[path[train][i-1]->pos].name,path[train][i-1]->TrainStopDist[train],print_debug);
                    //Print("Stop distance is [%ld] (index = [%ld]", path[train][nextStop]->TrainDistance[train] - path[train][i-1]->TrainDistance[train] - path[train][i-1]->TrainStopDist[train],i-1,0,print_debug);
                    path[train][i-1]->TrainStopDelay[train] = path[train][i-1]->TrainStopDist[train]*1000/v;
                    stoppingNode = i-1;
                    break;
                }else{
                    distLeft -= path[train][i]->TrainDistance[train] - path[train][i-1]->TrainDistance[train];
                    
                    path[train][i]->TrainIsAccelerating[train] = 1;
                }
            }
            
            //Print("Setting info for const speed nodes",0,0,0,print_debug);
            //set information for constant speed nodes
            for(int i = curNum;i < stoppingNode;++i){
                
                
                if(i != index) path[train][i]->TrainStartTime[train] = path[train][i-1]->TrainEndTime[train]; //minus 1 on both?
                if(path[train][i+1]->TrainDistance[train] - path[train][i]->TrainDistance[train] < 0) Print("Distance not setup well: [%ld] < [%ld]",path[train][i+1]->TrainDistance[train], path[train][i]->TrainDistance[train],0,print_debug);
                if(path[train][i]->TrainStopUntil[train] != -1){
                        path[train][i]->TrainEndTime[train] = path[train][i]->TrainStopUntil[train] + (path[train][i+1]->TrainDistance[train] - path[train][i]->TrainDistance[train])*1000/v; //double check this line
                }else{
                        path[train][i]->TrainEndTime[train] = path[train][i]->TrainStartTime[train] + (path[train][i+1]->TrainDistance[train] - path[train][i]->TrainDistance[train])*1000/v; //double check this line
                }

//                Print("Node [%ld] start [%ld] end [%ld]",i,path[train][i]->TrainStartTime[train],path[train][i]->TrainEndTime[train], print_debug);
                
            }
            //Print("No longer at const speed",0,0,0,print_debug);
            //now at stopping node, set for decelerating nodes
            
            if(stoppingNode != index) path[train][stoppingNode]->TrainStartTime[train] = path[train][stoppingNode-1]->TrainEndTime[train]; 
            
            
            for(int i = stoppingNode;i<nextStop;++i){
                
                path[train][i]->TrainStartTime[train] = path[train][stoppingNode]->TrainStartTime[train];
                if(path[train][i+1]->TrainDistance[train] - path[train][i]->TrainDistance[train] < 0) Print("Distance not setup well: [%ld] < [%ld]",path[train][i+1]->TrainDistance[train], path[train][i]->TrainDistance[train],0,print_debug);
                if(path[train][i]->TrainStopUntil[train] != -1){
                    path[train][i]->TrainEndTime[train] = path[train][stoppingNode]->TrainStopUntil[train] + decel_time + (path[train][stoppingNode+1]->TrainDistance[train] - path[train][stoppingNode]->TrainDistance[train] - distLeft)/v;
                }else{
                    path[train][i]->TrainEndTime[train] = path[train][stoppingNode]->TrainStartTime[train] + decel_time + (path[train][stoppingNode+1]->TrainDistance[train] - path[train][stoppingNode]->TrainDistance[train] - distLeft)/v;
                }
                //path[train][i]->TrainEndTime[train] = path[train][stoppingNode]->TrainStartTime[train] + decel_time + (path[train][stoppingNode+1]->TrainDistance[train] - path[train][stoppingNode]->TrainDistance[train] - distLeft)/v;
//                Print("Node [%ld] start [%ld] end [%ld]",i,path[train][i]->TrainStartTime[train],path[train][i]->TrainEndTime[train], print_debug);
                
            }
            
            
            path[train][nextStop]->TrainStartTime[train] = path[train][stoppingNode]->TrainEndTime[train];
//            Print("Node [%ld] start [%ld] end [%ld]",nextStop,path[train][nextStop]->TrainStartTime[train],path[train][nextStop]->TrainEndTime[train], print_debug);
                
            curNum = nextStop;
            v = 0;
            //Print("Done v == fullspeed",0,0,0,print_debug);
        }
    }

    

    path[train][len-1]->TrainIsAccelerating[train] = 0;
    
    if(path[train][len-1]->TrainStopUntil[train] != -1){
        path[train][len-1]->TrainEndTime[train] = path[train][len-1]->TrainStopUntil[train] + 100000;
    }else{
        path[train][len-1]->TrainEndTime[train] = path[train][len-1]->TrainStartTime[train] + 100000;
    }
//    for(int i = 0; i < len; ++i){
//        Print("Node [%ld] start [%ld] end [%ld]",i,path[train][i]->TrainStartTime[train],path[train][i]->TrainEndTime[train], print_debug);
//    }
    //Print("Done setup [%ld]",train,0,0,print_debug);
    
}

// Testing:

void move_dist_test(int train, int dist_mm, int speed) {

    long int clockTid; 
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }
    
    //long int move_dist(int dist_mm, long int *cmd_delay, int speed)
    long int cmd_delay;
    Print("move_dist test: total time[%ld], stop delay[%ld]", move_dist(dist_mm, &cmd_delay, speed, train), cmd_delay, 0, print_log);

    TrainSpeed(trainNums[train], speed);    
    Print("Train %d moving at speed[%ld]", train + 1, speed, 0, print_log);
    Delay(clockTid, cmd_delay);
    TrainSpeed(trainNums[train], 0);
    Print("Train %d stopping!", train + 1, speed, 0, print_log);

}

void accel_val_test(int train, int speed) {

    int dist_mm = 0;
    Print("accel_val test: stopping time[%ld], stopping dist[%d]", accel_val(&dist_mm, speed, train_speed[train][speed], 1, train), dist_mm, 0, print_log);
    Print("                accel time[%ld], accel dist[%d]", accel_val(&dist_mm, speed, 0, 0, train), dist_mm, 0, print_log);

}






void avoidCollision(int startingI, int train){
    int i1 = startingI;
    struct track_node *trackNode = &track[path[train][i1]->pos];
    struct dijNode *reverse; 
    struct dijNode *reverse2; 

    int start = 0;
    int end = 0;

    if(trackNode->type != NODE_BRANCH && trackNode->type != NODE_EXIT){
        start = DIR_AHEAD;
        end = DIR_AHEAD;
    }else if(trackNode->type == NODE_EXIT){
        return; //rip rip john
    }else{
        start = DIR_STRAIGHT;
        end = DIR_CURVED;
    }

    for(int dir = start; dir <= end; ++dir){
        reverse = &nodes[trackNode->edge[dir].reverse->src->idx];
        Print("Reverse of [%s] is [%s]",(long int) trackNode->name, (long int) track[reverse->pos].name,0,print_log);
        if( !(path[train][i1]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < path[train][i1]->TrainStartTime[1-train] || path[train][i1]->TrainStartTime[train] > path[train][i1]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE) ){
            //forwards collision (both trains going same direction)
            Print("forwards collision at [%ld]", i1, 0, 0, print_log);
            
            if(path[train][i1]->TrainStartTime[train] < path[train][i1]->TrainStartTime[1- train]){ //stopping other train
                for(int j = 0;j < pathLen[1-train]; ++j){
                    if(path[1-train][j] == path[train][i1]){
                        Print("Swapping trains 1",0,0,0,print_log);
                        avoidCollision(j,1-train);
                        return;
                    }
                }
            }

            int i = i1-1;
            int dist = path[train][i+1] - path[train][i];
            for(;i > 0 && dist < COLLISION_DIST_TOLERANCE;){
                --i;
                dist += path[train][i+1] - path[train][i];
            }
            path[train][i]->TrainStopUntil[train] = path[train][i1]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE; 
            Print("Setup Stopuntil at i [%ld], path[train][i1]->TrainEndTime[1-train] [%ld], plus tol = [%ld]",i,path[train][i1]->TrainEndTime[1-train], path[train][i]->TrainStopUntil[train], print_debug);
            if(path[train][i]->TrainStopUntil[train] == 49) Print("THIS IS THE ISSUE 0",0,0,0,print_log); 
            if(path[train][i]->TrainCommand[train] != COMMAND_STOP && path[train][i]->TrainCommand[train] != COMMAND_REVERSE){
                path[train][i]->TrainCommand[train] = COMMAND_STOP;
            }
            
            Print("Stopping at [%ld] until [%ld]",i,path[train][i]->TrainStopUntil[train],0,print_debug);
            
        }else if(!(path[train][i1]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < reverse->TrainStartTime[1-train] || path[train][i1]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)){
            //reverse collision (head on)
            Print("reverse collision at [%ld]",i1,0,0,print_log);
            int i = i1-1;
            struct track_node tempTrack;
            int stopUntilTime = reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE;
            for(;i >= 0;--i){
                tempTrack = track[path[train][i]->pos];
                if(tempTrack.type !=  NODE_BRANCH){
                    reverse = &nodes[tempTrack.edge[DIR_AHEAD].reverse->src->idx];
                    if( (path[train][i]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)
                    && (path[train][i]->TrainStartTime[train] > path[train][i]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)
                    ) break;
                    
                }else{
                    reverse = &nodes[tempTrack.edge[DIR_STRAIGHT].reverse->src->idx];
                    reverse2 = &nodes[tempTrack.edge[DIR_CURVED].reverse->src->idx];
                    if(( path[train][i]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)
                    && ( path[train][i]->TrainStartTime[train] > path[train][i]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)
                    && ( path[train][i]->TrainStartTime[train] > reverse2->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)
                   ) break;
                }
            }
            if(i < 0){
                Print("Need to immediately reverse to prevent collision",0,0,0,print_log);
                
                reverseInsert[0] = track[path[train][0]->pos].reverse->idx;
                int next = -1;
                int diverged = 0;
                int distSinceSplit = 0;
                int i = 1;
                for(;distSinceSplit < COLLISION_DIST_TOLERANCE;++i){
                    Print("i = [%ld], distSinceSplit = [%ld] ", i, distSinceSplit,0,print_debug);
                    if(track[reverseInsert[i-1]].type != NODE_BRANCH && track[reverseInsert[i-1]].type != NODE_EXIT){
                        next = track[reverseInsert[i-1]].edge[DIR_AHEAD].dest->idx;
                        if(diverged) distSinceSplit += track[reverseInsert[i-1]].edge[DIR_AHEAD].dist;
                    }else if(track[reverseInsert[i-1]].type == NODE_EXIT){
                        break;
                    }else{
                        if(nodes[track[reverseInsert[i-1]].edge[DIR_STRAIGHT].dest->idx].TrainStartTime[1-train] == -1){
                            next = track[reverseInsert[i-1]].edge[DIR_STRAIGHT].dest->idx;
                            if(diverged) distSinceSplit += track[reverseInsert[i-1]].edge[DIR_STRAIGHT].dist;
                        }else if(nodes[track[reverseInsert[i-1]].edge[DIR_CURVED].dest->idx].TrainStartTime[1-train] == -1){
                            next = track[reverseInsert[i-1]].edge[DIR_CURVED].dest->idx;
                            if(diverged) distSinceSplit += track[reverseInsert[i-1]].edge[DIR_CURVED].dist;
                        }else{
                            Print("!!!!!!!!!!!!!!!!!!BOTH BRANCHES ARE USED IN PATH?!?",0,0,0,print_log);
                            return;
                        }
                    }
                        
                    if(nodes[next].TrainStartTime[1-train] != -1){
                        diverged = 1;
                    }
                    reverseInsert[i] = next;
                    
                }
                
                for(int j = 0; j < i ; j++){
                    Print("Moving back to node [%ld], i=%ld", reverseInsert[j], i, 0, print_debug);
                    Print("   aka [%s] - 1", (long int) track[reverseInsert[j]].name, 0, 0, print_debug);
                }
                Print("Starting other way (ew ugly we hate this)",0,0,0,print_debug);
                int firstBatch = i;
                    
                //FIGURED OUT THIS PART                    
                for(int idx = firstBatch -1; idx > 0; --idx){
                    reverseInsert[i] = track[reverseInsert[idx]].reverse->idx;
                    i++; 
                }
                nodes[reverseInsert[firstBatch]].TrainStopUntil[train] = stopUntilTime;
                //shifting over    
                Print("shifting over",0,0,0,print_debug);
                for(int pos = pathLen[train]-1; pos >= 0;--pos ){
                    path[train][pos+i] = path[train][pos];
                }
                //adding nodes to start
                Print("adding nodes to start",0,0,0,print_debug);
                for(int pos = 0; pos < i;++pos){
                    path[train][pos] = &nodes[reverseInsert[pos]];
                }

                path[train][firstBatch-1]->TrainCommand[train] = COMMAND_REVERSE;

                //setting branch directions
                Print("setting branch directions",0,0,0,print_debug);
                for(int pos = 0; pos < i-1; ++pos){
                    if(track[path[train][pos]->pos].type == NODE_BRANCH){
                        if(track[path[train][pos]->pos].edge[DIR_STRAIGHT].dest->idx == path[train][pos+1]->pos ){
                            path[train][pos]->TrainCommand[train] = COMMAND_STRAIGHT;
                        }else if(track[path[train][pos]->pos].edge[DIR_CURVED].dest->idx == path[train][pos+1]->pos ){
                            path[train][pos]->TrainCommand[train] = COMMAND_CURVED;
                        }else{
                            Print("Branch [%ld] not leading to next: [%ld]",path[train][pos]->pos, path[train][pos+1]->pos,0,print_debug);
                        }
                    }
                }
                pathLen[train] += firstBatch*2-1;
                //recalclating path distance 
                recalcPathDist(train);

                Print("!!!!!!!!!!!!!!!!!!Flipping train [%ld] direction",train + 1,0,0,print_log);
                pure_train_reverse(trainNums[train]);
                TrainSpeed(trainNums[train],train_current_speed[train]);
                return;
                
            }else{
                Print("Setting up stop until at [%ld]",i,0,0,print_debug);
                path[train][i]->TrainStopUntil[train] = stopUntilTime; //still set from before break :)
                
                if(path[train][i]->TrainStopUntil[train] == 49) Print("THIS IS THE ISSUE 1",0,0,0,print_log); 
                Print("Setup Stopuntil at i [%ld], reverse->TrainEndTime[1-train] [%ld], plus tol = [%ld]",i,reverse->TrainEndTime[1-train], path[train][i]->TrainStopUntil[train], print_debug);
                
                if(path[train][i]->TrainCommand[train] != COMMAND_STOP || path[train][i]->TrainCommand[train] != COMMAND_REVERSE){
                    path[train][i]->TrainCommand[train] = COMMAND_STOP;
                }
                return;
            }

        }else{
            Print("Trying to avoid non existent collision at index[%ld] t1 end ",i1,0,0,print_log);
        }
    }    
}

int collisionPos(int train){
    //Print("starting collision pos",0,0,0,print_debug);
    for(int i = 0; i < pathLen[train];++i){
        struct track_node *trackNode = &track[path[train][i]->pos];
        struct dijNode *reverse; 

        if(trackNode->type != NODE_BRANCH && trackNode->type != NODE_EXIT){
            reverse = &nodes[trackNode->edge[DIR_AHEAD].reverse->src->idx];
            if(path[train][i]->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < path[train][i]->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > path[train][i]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE) ){
                return i;
            }else if(reverse->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < reverse->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)){
                return i;
            }
        }else if(trackNode->type == NODE_EXIT){
            //Print("Rip John",0,0,0,print_debug);
        }else{
            reverse = &nodes[trackNode->edge[DIR_STRAIGHT].reverse->src->idx];
            if(path[train][i]->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < path[train][i]->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > path[train][i]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE) ){
                return i;
            }else if(reverse->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < reverse->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)){
                return i;
            }
            reverse = &nodes[trackNode->edge[DIR_CURVED].reverse->src->idx];
            if(path[train][i]->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < path[train][i]->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > path[train][i]->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE) ){
                return i;
            }else if(reverse->TrainStartTime[1-train] != -1 && !(path[train][i]->TrainEndTime[train] + COLLISION_TIME_TOLERANCE < reverse->TrainStartTime[1-train] || path[train][i]->TrainStartTime[train] > reverse->TrainEndTime[1-train] + COLLISION_TIME_TOLERANCE)){
                return i;
            }
        }
            
    }
    return -1;
}

int recalculate(int speed) {
    
    isCalculating[0] = 1;
    isCalculating[1] = 1;
    newSetup(pathIndex[0], 0, speed, train_speed[0][train_current_speed[0]]);
    newSetup(pathIndex[1], 1, speed, train_speed[1][train_current_speed[1]]);
    int x;
    int count = 0;
    int idx = 0;
    while((x = collisionPos(idx)) != -1){
        Print("Trying to avoid collision at %s", (long int)track[path[idx][x]->pos].name, 0, 0, print_log);
        if (count > 40) {
            isCalculating[0] = 0;
            isCalculating[1] = 0;
            Print("Pulling a sneaky", 0, 0, 0, print_log);
            return -1;     
        }
        if(count > 20) {
            idx = 1-idx;
            Print("Count exceeded 20, swapping swapping priority to %s", idx, 0, 0, print_log);
            newSetup(pathIndex[0], 0, speed, train_speed[0][train_current_speed[0]]);
            newSetup(pathIndex[1], 1, speed, train_speed[1][train_current_speed[1]]);
        }
        avoidCollision(x, 0);        
        Print("Remeasuring after fixing collision at %s", (long int)track[path[idx][x]->pos].name, 0, 0, print_log);
        newSetup(pathIndex[0], 0, speed, train_speed[0][train_current_speed[0]]);
        newSetup(pathIndex[1], 1, speed, train_speed[1][train_current_speed[1]]);
    //printPath(0);
    //printPath(1);
//        printNodes(0);
//        printNodes(1);
        ++count;

    }
    Print("Finished determining path",0,0,0,print_debug);
    
    printPath(0);
    printPath(1);
    
    isCalculating[0] = 0;
    isCalculating[1] = 0;

    return 0;
}
