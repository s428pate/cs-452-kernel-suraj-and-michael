#include "activate.h"
#include "common.h"
#include "printf.h"

//globals to pass state to and from kernel and user space
long int retval;
long int g_p1;
long int g_p2;
long int g_p3;
long int g_p4;
long int g_p5;
char *g_sp;
long int g_spsr;
void (*g_pc)();

void printReg(long int reg) {
  printf("reg: [%lu|%x]\n\r", reg, reg);
}

extern void kernel_exit();

enum request activate(struct task_descriptor * td) {
  //printf("   Activating [%d]\n\r", td->TID);
  
  retval = td->retval;
  g_p1 = td->p1;
  g_p2 = td->p2;
  g_p3 = td->p3;
  g_p4 = td->p4;
  g_p5 = td->p5;
  g_sp = td->sp;
  g_spsr = td->spsr;
  g_pc = td->pc;
  
//printf("entering[%ld]\n\r\033[37m", curr->TID);

  kernel_exit();

//printf("\033[33mexiting[%ld] for [%ld]\n\r", curr->TID, retval & 0xFFFF);
// note: prints after terminal_transmit interrupt breaks it

  retval = retval & 0xFFFF; //lowest 16 bits for SVC N

  curr->pc = g_pc;
  curr->spsr = g_spsr;
  curr->sp = g_sp;
  curr->p5 = g_p5;
  curr->p4 = g_p4;
  curr->p3 = g_p3;
  curr->p2 = g_p2;
  curr->p1 = g_p1;
  curr->retval = g_p1; //default retval of x0

  //printf("   Deactivating [%d]\n\r", curr->TID);
  
  return retval;
}
