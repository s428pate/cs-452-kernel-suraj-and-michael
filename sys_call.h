#ifndef _SYS_CALL_H_
#define _SYS_CALL_H_

//
//  For Userspace to call
//
//      Note: If commented out, it's in sys_all_wrapper.h
//

extern long int Create(long int priority, void (*function)());
/*
allocates and initializes a task descriptor, using the given priority, 
and the given function pointer as a pointer to the entry point of executable 
code, essentially a function with no arguments and no return value. When Create 
returns, the task descriptor has all the state needed to run the task, the 
task’s stack has been suitably initialized, and the task has been entered into 
its ready queue so that it will run the next time it is scheduled.

Return Value:

    tid	    the positive integer task id of the newly created task. The task id 
            must be unique, in the sense that no other active task has the same
            task id.

    -1	    invalid priority.

    -2	    kernel is out of task descriptors.

Comment: If newly created tasks need to receive configuration parameters, these
         generally need to be provided via message passing. However, if the 
         configuration value is a primitive type, implementing a shortcut via 
         another argument to Create() and function() is permissible. 
*/


extern long int MyTid();
/*
returns the task id of the calling task.

Return Value:

    tid	    the positive integer task id of the task that calls it.

Comment: Errors should be impossible! 
*/


extern long int MyParentTid();
/*
returns the task id of the task that created the calling task. This will be 
problematic only if the task has exited or been destroyed, in which case the
return value is implementation-dependent.

Return Value:

    tid	    the task id of the task that created the calling task.

Comment: The return value is implementation-dependent, if the parent has exited,
         has been de- stroyed, or is in the process of being destroyed. 
*/


extern void Yield();
/*
causes a task to pause executing. The task is moved to the end of its priority
queue, and will resume executing when next scheduled.

Comment: Like every entry to the kernel, Yield() reschedules. 
*/


extern void Exit();
/*
causes a task to cease execution permanently. It is removed from all priority
queues, send queues, receive queues and event queues. Resources owned by the 
task, primarily its memory and task descriptor, are not reclaimed.

Comment: Exit does not return at all. If a point occurs where all tasks have
         exited, the kernel should return cleanly to RedBoot. 
*/



extern long int Send(long int tid, const char *msg, long int msglen, char *reply, long int replylen);
/*
sends a message to another task and receives a reply. The message, in a buffer
in the sending task’s memory, is copied to the memory of the task to which it
is sent by the kernel. Send() supplies a buffer into which the reply is to be
copied, and the size of the reply buffer, so that the kernel can detect overflow. 
When Send() returns without error it is guaranteed that the message has been 
received, and that a reply has been sent, not necessarily by the same task. The
kernel will not overflow the reply buffer. If the size of the reply set exceeds
rplen, the reply is truncated and the buffer contains the first rplen characters
of the reply. The caller is expected to check the return value and to act 
accordingly. There is no guarantee that Send() will return. If, for example, the 
task to which the message is directed never calls Receive(), Send() never returns
and the sending task remains blocked forever. Send() has a passing resemblance, 
and no more, to a remote procedure call.

Return Value:
   >-1	    the size of the message returned by the replying task. The actual 
                reply is less than or equal to the size of the reply buffer 
                provided for it. Longer replies are truncated.
    -1	    tid is not the task id of an existing task.
    -2	    send-receive-reply transaction could not be completed. 
*/


extern long int Receive(long int *tid, char *msg, long int msglen);
/*
blocks until a message is sent to the caller, then returns with the message in 
its message buffer and tid set to the task id of the task that sent the message. 
Messages sent before Receive() is called are retained in a send queue, from 
which they are received in first-come, first-served order. The argument msg must
point to a buffer at least as large as msglen. The kernel will not overflow the 
message buffer. If the size of the message set exceeds msglen, the message is 
truncated and the buffer contains the first msglen characters of the message 
sent. The caller is expected to check the return value and to act accordingly.

Return Value:
   >-1	    the size of the message sent by the sender (stored in tid). The 
                actual message is less than or equal to the size of the message 
                buffer supplied. Longer messages are truncated. 
*/


extern long int Reply(long int tid, void *reply, long int replylen);
/*
sends a reply to a task that previously sent a message. When it returns without 
error, the reply has been copied into the sender’s memory. The calling task and 
the sender return at the same logical time, so whichever is of higher priority 
runs first. If they are of the same priority, the sender runs first.

Return Value:
   >-1	    the size of the reply message transmitted to the original sender 
                task. If this is less than the size of the reply message, the 
                message has been truncated.
    -1	    tid is not the task id of an existing task.
    -2	    tid is not the task id of a reply-blocked task. 
*/



//
//  long int RegisterAs(const char *name);
/*
registers the task id of the caller under the given name. On return without error 
it is guaranteed that all WhoIs() calls by any task will return the task id of the 
caller until the registration is overwritten. If another task has already 
registered with the given name, its registration is overwritten. RegisterAs() is 
actually a wrapper covering a send to the name server.

Return Value:
    0	    success.
    -1	    invalid name server task id inside wrapper. 
*/

//
//  long int WhoIs(const char *name);
/*
    asks the name server for the task id of the task that is registered under the 
    given name. Whether WhoIs() blocks waiting for a registration or returns with 
    an error, if no task is registered under the given name, is 
    implementation-dependent. There is guaranteed to be a unique task id associated 
    with each registered name, but the registered task may change at any time after 
    a call to WhoIs(). WhoIs() is actually a wrapper covering a send to the name 
    server.

    Return Value:
    tid	    task id of the registered task.
    -1	    invalid name server task id inside wrapper. 
*/

extern long int AwaitEvent(long int event);


extern long int Shutdown();


extern long int UartWriteRegister(long int channel, long int reg, long int data);

extern long int UartReadRegister(long int channel, long int reg);

//Only 1 at a time
extern long int TrainGetC();

//Warning: Will break stuff if not ultra careful!
//         Currently only in use for printing before print server running
extern long int KernelPrint(char *str, long int val1, long int val2, long int val3, long int val4);

#endif