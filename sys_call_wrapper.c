#include "sys_call_wrapper.h"
#include "sys_call.h"
#include "common.h"
#include "scheduler.h"
#include "train_switch_server.h"
#include "printf.h"


#define CURSOR_SEPERATOR_1 "\033[2;1H"

#define CURSOR_TIME        "\033[3;1H"
#define CURSOR_IDLE        "\033[4;1H"
#define CURSOR_TIMEOUTS    "\033[5;1H"

#define CURSOR_SEPERATOR_2 "\033[6;1H"

#define CURSOR_SENSORS     "\033[7;1H"
#define CURSOR_SENSOR1     "\033[7;17H" 
#define CURSOR_SENSOR2     "\033[7;21H"
#define CURSOR_SENSOR3     "\033[7;25H"
#define CURSOR_SENSOR4     "\033[7;29H"
#define CURSOR_SENSOR5     "\033[7;33H"

#define CURSOR_SEPERATOR_3 "\033[8;1H"

#define CURSOR_STATES1     "\033[9;1H"
#define CURSOR_STATES2     "\033[10;1H"
#define CURSOR_STATES3     "\033[11;1H"
#define CURSOR_STATES4     "\033[12;1H"
#define CURSOR_STATES5     "\033[13;1H"
#define CURSOR_STATES6     "\033[14;1H"
#define CURSOR_STATES7     "\033[15;1H"
#define CURSOR_STATES8     "\033[16;1H"

#define CURSOR_SEPERATOR_4 "\033[18;1H"

#define CURSOR_PATH1       "\033[19;1H"
#define CURSOR_PATH2       "\033[20;1H"
#define CURSOR_DIFF        "\033[21;1H"

#define CURSOR_SEPERATOR_5 "\033[23;1H"

#define CURSOR_INPUT_OLD   "\033[24;1H"
#define CURSOR_INPUT       "\033[25;1H"

#define CURSOR_SEPERATOR_6 "\033[26;1H"

#define CURSOR_INPUT_HELP  "\033[27;1H"
//input help is 2 lines          28

#define CURSOR_SEPERATOR_7 "\033[30;1H"

//Logs: 31 - 49

//#define CURSOR_SEPERATOR_8 "\033[50;1H"
#define CURSOR_SEPERATOR_8 "\033[68;1H"

#define CURSOR_HIDE        "\033[?25l"
#define CURSOR_SAVE        "\0337"
#define CURSOR_RESTORE     "\0338"
#define LINE_CLEAR         "\033[K" 
#define SCREEN_CLEAR       "\033[2J"
#define TEXT_RED           "\033[31m"
#define TEXT_GREEN         "\033[32m"
#define TEXT_YELLOW        "\033[33m"
#define TEXT_BLUE          "\033[34m"
#define TEXT_MAGENTA       "\033[35m"
#define TEXT_CYAN          "\033[36m"
#define TEXT_WHITE         "\033[37m"

#define SEPERATOR "------------------------------------------------------------------------------"
#define SECTION_SEPERATOR1 "===============================\033[37m General Status \033[36m==============================="
#define SECTION_SEPERATOR2 "===============================\033[37m Trains Control \033[36m==============================="
#define SECTION_SEPERATOR3 "==================================\033[37m Terminal \033[36m=================================="
#define SECTION_SEPERATOR4 "================================\033[37m Log Messages \033[36m================================"
#define SECTION_SEPERATOR5 "=============================================================================="

static unsigned long int log_count = 1;

// Name Server
//  Assumes Name server created first

long int RegisterAs(const char *name) {
    char msg[MESSAGE_MAX_SIZE];
    msg[0] = 'R';
    msg[1] = 'e';
    msg[2] = 'g';
    long int size = 0;
    for(; name[size] != '\0'; ++size) {
        msg[size + 3] = name[size];
    }
    msg[size + 3] = '\0';
    size += 1 + 3;
    
    long int reply;
    if (Send(0, msg, size, (char *) &reply, sizeof(reply)) < 0) return -1;
    
    return 0;
}

long int WhoIs(const char *name) {
    char msg[MESSAGE_MAX_SIZE];
    msg[0] = 'W';
    msg[1] = 'h';
    msg[2] = 'o';
    long int size = 0;
    for(; name[size] != '\0'; ++size) {
        msg[size + 3] = name[size];
    }
    msg[size + 3] = '\0';
    size += 1 + 3;

    long int reply;
    if (Send(0, msg, size, (char *) &reply, sizeof(reply)) < 0) return -1;
    
    return reply;
}


//Clock Server:

long int Time(long int tid){
    char msg[MESSAGE_MAX_SIZE];
    if(tid != WhoIs("Clock Server")) return -1;
    msg[0] = 'T';
    msg[1] = 'i';
    msg[2] = 'm';
    msg[3] = 'e';

    long int reply;
    if (Send(tid, msg, 4, (char *) &reply, sizeof(reply)) < 0) return -1;
    
    return reply; 

}

long int Delay(long int tid, int delay){
    char msg[MESSAGE_MAX_SIZE];
    long int clockTid;
    while ((clockTid = WhoIs("Clock Server")) == -1 || clockTid == 255) { Yield(); }

    if (delay < 0) return -2;
    if (tid != clockTid) {
        Print("wrong tid[%ld] from [%ld] (Clock Server tid[%ld])", tid, MyTid(), clockTid, print_log);
        return -1;
    }

    msg[0] = 'D';
    msg[1] = 'e';
    msg[2] = 'l';
    msg[3] = '\0';
    
    for (long unsigned int i = 0; i < sizeof(int); ++i) {
        msg[4 + i] = *((char *)(&delay) + i);
    }

    long int reply;
    long int temp = Send(tid, msg, 4+sizeof(int), (char *) &reply, sizeof(reply));
    
    if (temp < 0) {
        Print("send error[%ld]", temp, 0, 0, print_log);
        return -1;
    }
    
    return reply; 

}

long int DelayUntil(long int tid, int delay){
    char msg[MESSAGE_MAX_SIZE];
    if (delay < 0) return -2;
    if(tid != WhoIs("Clock Server")) return -1;

    msg[0] = 'D';
    msg[1] = 'e';
    msg[2] = 'l';
    msg[3] = 'U';

    for (long unsigned int i = 0; i < sizeof(int); ++i) {
        msg[4 + i] = *((char *)(&delay) + i);
    }

    long int reply;
    if (Send(tid, msg, 4+sizeof(int), (char *) &reply, sizeof(reply)) < 0) return -1;
    
    return reply; 
}

//

int Getc(long int tid, int uart) {

    char msg[4];
    char reply;

    if (uart == terminal_channel) {
        if (tid != WhoIs("TerminalRT Server")) {
            Print("GetC for terminal with wrong tid", 0, 0, 0, print_log);
            return 0;
        }

        msg[0] = 'G';
        msg[1] = 'e';
        msg[2] = 't';
        msg[3] = 'C';

        Send(tid, msg, 4, &reply, sizeof(reply));
        return reply;

    } else {
        return TrainGetC();
    }
}

int Putc(long int tid, int uart, char ch) {

    if (uart == terminal_channel) {
        if (tid != WhoIs("TerminalRT Server")) return -1;
        Print("%c", ch, 0, 0, print_log);

    } else if (uart == train_channel) {
        if (tid != WhoIs("TrainRT Server")) return -1;
        
        char reply;
        char msg[4];
        msg[0] = 'c';
        msg[1] = 'm';
        msg[2] = 'd';
        msg[3] = ch;
        
        Send(tid, msg, 4, &reply, sizeof(reply));
    }
    return 0;
}

//Terminal Interface

//assumes null terminated
void str_cpy(char *dest, char *src) {
    long int size = 0;
    for (size = 0; src[size] != '\0'; ++size) {
        dest[size] = src[size];
    }
    dest[size] = src[size];
}


void Print(char * msg, long int val1, long int val2, long int val3, long int type) {
    
    long int terminal_tid;
    while ((terminal_tid = WhoIs("TerminalRT Server")) == -1) { Yield(); }
    
    int size;
    long int reply;

    char pos[sizeof("\033[XX;XXH")];
    char col[sizeof("\033[XXm")];
    char clr[sizeof("\033[K")];
    char tempBuf[MESSAGE_MAX_SIZE];
    char buf[MESSAGE_MAX_SIZE];

    buf[0] = 'P';
    buf[1] = 'r';
    buf[2] = 'i';
    buf[3] = 'n';
    buf[4] = 't';

    sprintf(tempBuf, msg, val1, val2, val3);

    switch (type) {
        
        case print_prev_input:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_INPUT_OLD);
            str_cpy(col, TEXT_WHITE);
            break;

        case print_input:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_INPUT);
            str_cpy(col, TEXT_WHITE);
            break;

        case print_input_help:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_INPUT_HELP);
            str_cpy(col, TEXT_RED);
            break;

        case print_time:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_TIME);
            str_cpy(col, TEXT_YELLOW);
            break;
        
        case print_idle:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_IDLE);
            str_cpy(col, TEXT_GREEN);
            break;
            
        case print_timeouts:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_TIMEOUTS);
            str_cpy(col, TEXT_CYAN);
            break;

        case print_path_i + 0:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_PATH1);
            str_cpy(col, TEXT_RED);
            break;

        case print_path_i + 1:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_PATH2);
            str_cpy(col, TEXT_RED);
            break;

        case print_train_diff:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_DIFF);
            str_cpy(col, TEXT_YELLOW);
            break;

        case print_switch_i + 0: //1-3
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES1);
            str_cpy(col, TEXT_MAGENTA);
            break;
            
        case print_switch_i + 3: //4-6
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES2);
            str_cpy(col, TEXT_YELLOW);
            break;

        case print_switch_i + 6: //7-9
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES3);
            str_cpy(col, TEXT_YELLOW);
            break;
            
        case print_switch_i + 9: //10-12
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES4);
            str_cpy(col, TEXT_YELLOW);
            break;
            
        case print_switch_i + 12: //13-15
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES5);
            str_cpy(col, TEXT_YELLOW);
            break;
        
        case print_switch_i + 15: //16-18
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES6);
            str_cpy(col, TEXT_YELLOW);
            break;
            
        case print_switch_i + 18: //153-154
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES7);
            str_cpy(col, TEXT_YELLOW);
            break;
        
        case print_switch_i + 20: //155-156
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_STATES8);
            str_cpy(col, TEXT_YELLOW);
            break;
            
        case print_sensor_i:
            str_cpy(clr, LINE_CLEAR);
            str_cpy(pos, CURSOR_SENSORS);
            str_cpy(col, TEXT_RED);
            break;
            
        case print_sensor_i + 1:
            str_cpy(clr, "");
            str_cpy(pos, CURSOR_SENSOR1);
            str_cpy(col, TEXT_MAGENTA);
            break;
            
        case print_sensor_i + 2:
            str_cpy(clr, "");
            str_cpy(pos, CURSOR_SENSOR2);
            str_cpy(col, TEXT_MAGENTA);
            break;
            
        case print_sensor_i + 3:
            str_cpy(clr, "");
            str_cpy(pos, CURSOR_SENSOR3);
            str_cpy(col, TEXT_MAGENTA);
            break;
            
        case print_sensor_i + 4:
            str_cpy(clr, "");
            str_cpy(pos, CURSOR_SENSOR4);
            str_cpy(col, TEXT_MAGENTA);
            break;
            
        case print_sensor_i + 5:
            str_cpy(clr, "");
            str_cpy(pos, CURSOR_SENSOR5);
            str_cpy(col, TEXT_MAGENTA);
            break;

        case print_seperators:
            size = 5 + sprintf(buf + 5, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",   CURSOR_SAVE, 
                                                             TEXT_CYAN, CURSOR_SEPERATOR_1, SECTION_SEPERATOR1, 
                                                             TEXT_BLUE, CURSOR_SEPERATOR_2, SEPERATOR, 
                                                             TEXT_BLUE, CURSOR_SEPERATOR_3, SEPERATOR,
                                                             TEXT_CYAN, CURSOR_SEPERATOR_4, SECTION_SEPERATOR2, 
                                                             TEXT_CYAN, CURSOR_SEPERATOR_5, SECTION_SEPERATOR3, 
                                                             TEXT_BLUE, CURSOR_SEPERATOR_6, SEPERATOR,
                                                             TEXT_CYAN, CURSOR_SEPERATOR_7, SECTION_SEPERATOR4,
                                                             TEXT_CYAN, CURSOR_SEPERATOR_8, SECTION_SEPERATOR5,
                                                                                            CURSOR_RESTORE);
            Send(terminal_tid, buf, size, (char *)&reply, sizeof(reply)); 
            return;

#ifndef DEBUG_LOGS
        case print_debug:
            return;
#endif

        case print_log:
        default:
            size = 5 + sprintf(buf + 5, "\n\r%s[Log %02lu] %s%s", TEXT_BLUE, log_count, TEXT_WHITE, tempBuf);
            ++log_count;
            Send(terminal_tid, buf, size, (char *)&reply, sizeof(reply));
            return;
    }

    size = 5 + sprintf(buf + 5, "%s%s%s%s%s%s", CURSOR_SAVE, pos, clr, col, tempBuf, CURSOR_RESTORE);

    Send(terminal_tid, buf, size, (char *)&reply, sizeof(reply)); 
}


//Train Interface

#define STRAIGHT_CODE   33
#define CURVED_CODE     34
#define STOP_CODE       (0 + 16)
#define REVERSE_CODE    (15 + 16)

int TrainSpeed(long int train, long int speed) {

    long int train_status_server_tid;
    long int trainRT_server_tid;
    while ((train_status_server_tid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }

    if (train < 1 || 80 < train || speed < 0 || 14 < speed ) {
        return -1;
    }

    long int reply;

    char msg[7];
    msg[0] = 'S';
    msg[1] = 'e';
    msg[2] = 't';
    msg[3] = 's';
    msg[4] = 'p';
    msg[5] = train;
    msg[6] = speed;
    
    Send(train_status_server_tid, msg, 7, (char *)&reply, sizeof(reply));

    msg[0] = 'c';
    msg[1] = 'm';
    msg[2] = 'd';
    msg[3] = speed + 16;
    msg[4] = train;
    
    Send(trainRT_server_tid, msg, 5, (char *)&reply, sizeof(reply));

    return 0;
}

void pure_train_reverse(int train){
    long int train_status_server_tid;
    long int trainRT_server_tid;
    long int clock_server_tid;
    while ((train_status_server_tid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }
    char msg[7];
    char reply;
    msg[0] = 'c';
    msg[1] = 'm';
    msg[2] = 'd';
    msg[3] = REVERSE_CODE;
    msg[4] = train;
    Send(trainRT_server_tid, msg, 7, &reply, sizeof(reply));
}

void train_reverse_task() {
    
    long int tid;
    long int train_status_server_tid;
    long int trainRT_server_tid;
    long int clock_server_tid;
    while ((train_status_server_tid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }

    char msg[7];
    char reply;

    //get train
    char train;
    Receive(&tid, &train, sizeof(char));
    Reply(tid, &reply, sizeof(long int));

    //stop train
    msg[0] = 'c';
    msg[1] = 'm';
    msg[2] = 'd';
    msg[3] = STOP_CODE;
    msg[4] = train;
    Send(trainRT_server_tid, msg, 5, &reply, sizeof(reply));

    //Wait 4.3s
    Delay(clock_server_tid, REVERSE_DELAY);
    
    //get speed
    char speed;
    msg[0] = 'G';
    msg[1] = 'e';
    msg[2] = 't';
    msg[3] = 's';
    msg[4] = 'p';
    msg[5] = train;
    Send(train_status_server_tid, msg, 6, &speed, sizeof(speed));

    //Reverse train
    msg[0] = 'c';
    msg[1] = 'm';
    msg[2] = 'd';
    msg[3] = REVERSE_CODE;
    msg[4] = train;

    //set train speed
    msg[5] = speed + 16;
    msg[6] = train;
    Send(trainRT_server_tid, msg, 7, &reply, sizeof(reply));

    Exit();
}

int TrainReverse(long int train) {

    if (train < 1 || 80 < train) {
        return -1;
    }

    char train_char = train;
    long int reply;

    Send(Create(1, train_reverse_task), &train_char, sizeof(char), (char *)&reply, sizeof(long int));

    return 0;
}

int TrainSwitch(long int turnout, char direction) {

    if (direction != 'S' && direction != 'C') return -1;

    int straight = direction == 'S';
    
    if ((turnout == 153 && straight) || (turnout == 154 && !straight)) {
        set_train_switch(153, STRAIGHT_CODE);
        set_train_switch(154, CURVED_CODE);

    } else if ((turnout == 154 && straight) || (turnout == 153 && !straight)) {
        set_train_switch(153, CURVED_CODE);
        set_train_switch(154, STRAIGHT_CODE);

    } else if ((turnout == 155 && straight) || (turnout == 156 && !straight)) {
        set_train_switch(155, STRAIGHT_CODE);
        set_train_switch(156, CURVED_CODE);

    } else if ((turnout == 156 && straight) || (turnout == 155 && !straight)) {
        set_train_switch(155, CURVED_CODE);
        set_train_switch(156, STRAIGHT_CODE);

    } else if (1 <= turnout && turnout <= 18) {
        set_train_switch(turnout, (straight) ? STRAIGHT_CODE : CURVED_CODE);
        
    } else {
        return -1;
        
    }

    return 0;
}
