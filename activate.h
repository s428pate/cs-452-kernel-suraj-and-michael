#ifndef _ACTIVATE_H_
#define _ACTIVATE_H_

#include "common.h"
#include <stdint.h>


//globals to pass state to and from kernel and user space
extern long int retval;
extern long int g_p1;
extern long int g_p2;
extern char *g_sp;
extern long int g_spsr;
extern void (*g_pc)();



enum request activate(struct task_descriptor * td);

#endif