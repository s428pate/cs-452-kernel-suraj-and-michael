#include "train_switch_server.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "common.h"


#define SWITCH_BUFF_SIZE 256
#define SOLENOID_OFF 32
#define STRAIGHT_CODE 33
#define CURVED_CODE 34


void train_switch_server() {
    Print("Starting Switch Server", 0, 0, 0, print_log);
    
    RegisterAs("Train Switch Server");

    long int switch_buf[SWITCH_BUFF_SIZE];
    long int pos_buf[SWITCH_BUFF_SIZE];
    long int buf_start = 0;
    long int buf_end = 0;

    long int switchNotifierTid;
    long int trainRT_server_tid; 

    long int receiveTid;
    char reply;

    char msgBuf[MESSAGE_MAX_SIZE];
    char msg[7];


    //int readyToSend = 0;
    int off = 1;
    int first = 1;
    while ((switchNotifierTid = WhoIs("Train Switch Notifier")) == -1) { Yield(); }
    while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }

    for (;;) {
        
        Receive(&receiveTid, msgBuf, MESSAGE_MAX_SIZE);
        
        if(receiveTid == switchNotifierTid){
            if(first) {first = 0; continue;}
            if(buf_start != buf_end){
                msg[0] = 'c';
                msg[1] = 'm';
                msg[2] = 'd';
                msg[3] = pos_buf[buf_start];
                msg[4] = switch_buf[buf_start];

                buf_start = (buf_start + 1) % SWITCH_BUFF_SIZE;

                //set train speed
                
                Send(trainRT_server_tid, msg, 5, &reply, sizeof(reply));
                
                Reply(switchNotifierTid, &reply, sizeof(char));
                off = 0;
            }else{
                msg[0] = 'c';
                msg[1] = 'm';
                msg[2] = 'd';
                msg[3] = SOLENOID_OFF;
                //send turn off switch command
                Send(trainRT_server_tid, msg, 4, &reply, sizeof(reply));
                off = 1;

            }

        }

        else if (msgBuf[0] == 's' && msgBuf[1] == 'w') {
            switch_buf[buf_end] = msgBuf[2];
            pos_buf[buf_end] = msgBuf[3]; 
            buf_end = (buf_end + 1) % SWITCH_BUFF_SIZE;

            Reply(receiveTid,&reply,1);

            if(off){
                Reply(switchNotifierTid, &reply, sizeof(char));
            }
            

        } else {
            Print("Invalid switch server command [%.6s]", (long int)msgBuf, 0, 0, print_log);
        }
    }

}

void train_switch_notifier() {
    Print("Starting Switch Notifer", 0, 0, 0, print_log);
    
    RegisterAs("Train Switch Notifier");

    long int reply;
    long int train_switch_server_tid;
    long int clock_server_tid;
    while ((train_switch_server_tid = WhoIs("Train Switch Server")) == -1) { Yield(); }
    while ((clock_server_tid = WhoIs("Clock Server")) == -1) { Yield(); }

    for (;;) {
        Send(train_switch_server_tid, "notify", sizeof("notify"), (char *)&reply, sizeof(long int));
        Delay(clock_server_tid, SWITCH_DELAY);
    }
}

void set_train_switch(char turnout, char direction_byte) {

    long int train_status_server_tid;
    long int train_switch_server_tid;
    while ((train_status_server_tid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((train_switch_server_tid = WhoIs("Train Switch Server")) == -1) { Yield(); }

    long int reply;
    
    char msg[7];
    msg[0] = 'S';
    msg[1] = 'e';
    msg[2] = 't';
    msg[3] = 's';
    msg[4] = 'w';
    msg[5] = turnout;
    msg[6] = direction_byte;
    
    Send(train_status_server_tid, msg, 7, (char *)&reply, sizeof(reply));

    
    msg[0] = 's';
    msg[1] = 'w';
    msg[2] = turnout;
    msg[3] = direction_byte;
    Send(train_switch_server_tid, msg, 5, (char *)&reply, sizeof(reply));

}
