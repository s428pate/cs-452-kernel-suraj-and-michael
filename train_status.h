#ifndef _TRAIN_STATUS_H_
#define _TRAIN_STATUS_H_

#include "common.h"

extern struct track_node *mytrack;
extern struct dijNode *path[NUMTRAINS][144];
extern int pathLen[NUMTRAINS];
extern struct track_node t1;
extern int offset;

enum commands{
    COMMAND_NONE = 0,
    COMMAND_STRAIGHT = 1,
    COMMAND_CURVED = 2,
    COMMAND_REVERSE = 3,
    COMMAND_STOP = 4
};

struct dijNode{
    int pos;
    
    int prev;
    int visited;
    int postTurnoutDist;
    //int time; //rip tim //rip rip tim
    //also rip Mic (since he gets shot in chicago)
    int TrainDistance[NUMTRAINS];
    int TrainCommand[NUMTRAINS];
    int TrainStartTime[NUMTRAINS];
    int TrainEndTime[NUMTRAINS];
    int TrainStop[NUMTRAINS];
    int TrainStopDist[NUMTRAINS];
    int TrainIsStop[NUMTRAINS];
    int TrainIsStartStop[NUMTRAINS];
    int TrainStopUntil[NUMTRAINS];
    int TrainStopDelay[NUMTRAINS];
    int TrainIsReverse[NUMTRAINS];
    int TrainIsAccelerating[NUMTRAINS];
    int TrainDir[NUMTRAINS];
};

void trainStatusServer();

void train_switch_setter();

void dijkstras(long int start, long int startOffset, long int dest, long int destOffset, int train);

void traverse_path(char speed, int train, char isAuto);

void newSetup(int index, int train, int full_speed, int cur_velocity);

//int node_parse(char * str,int * pos);

void auto_dest();

void printNodes(int train);
int recalculate(int speed);

// Testing:
void move_dist_test(int train, int dist_mm, int speed);
void accel_val_test(int train, int speed);

#endif