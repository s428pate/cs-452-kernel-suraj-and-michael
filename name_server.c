#include "name_server.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "common.h"

//Assume names end with '\0'

long int str_match(char *names, char *buf, long int size) {
    if (size == 0) {
        return 0;
    }
    
    for (long int i = 0; i < size; ++i) {
        if (names[i] != buf[i]) {
            return 0;
        }
    }
    if (names[size - 1] != '\0' || buf[size - 1] != '\0') return 0; //check for lack of ending '\0'

    return 1;
}

void name_server() {

    //Note: Cannot print as print server relies on name server
    KernelPrint("Starting Name Server\n\r", 0, 0, 0, 0);

    char names[NUMTASKS][MESSAGE_MAX_SIZE];
    char msg_buf[MESSAGE_MAX_SIZE];
    long int size;
    long int tid;
    long int reply;

    for (long int i = 0; i < NUMTASKS; ++i) {
        names[i][0] = '\0';
    }

    for (;;) {
        //Receive request:
            size = Receive(&tid, msg_buf, MESSAGE_MAX_SIZE);

        //Process request:
          //Invalid
            if (size <= 4) {
                reply = -2;
            }
            
          //RegisterAs()
            else if (msg_buf[0] == 'R' && msg_buf[1] == 'e' && msg_buf[2] == 'g') {  

              //Remove previous refrences to same name
                for (long int i = 0; i < NUMTASKS; ++i)  {
                    if (str_match(msg_buf + 3, names[i], size - 3)) {
                        names[i][0] = '\0';
                    }
                }
                
              //Add name under tid
                for (long int i = 3; i < size; ++i) {
                    names[tid][i - 3] = msg_buf[i];
                }

              //Send reply = 0
                reply = 0;
            } 
            
          //WhoIs()
            else if (msg_buf[0] == 'W' && msg_buf[1] == 'h' && msg_buf[2] == 'o') {
                reply = -1;
                for (long int i = 0; i < NUMTASKS; ++i)  {
                    if (str_match(msg_buf + 3, names[i], size - 3)) {
                        reply = i;
                        continue;
                    }
                }
            }


        //Reply:
            Reply(tid, (char *)&reply, sizeof(reply));

    }

    //No manual Exit() since kernel is in charge of terminating name_server
}
