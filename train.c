#include "train.h"
#include "common.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"



void trainServer(){
    Print("Starting Train Server", 0, 0, 0, print_log);

    RegisterAs("TrainRT Server");

    long int receiveNotifierTid; 
    long int transmitNotifierTid; 
    long int statusTid; 
    long int clockTid;
    while ((receiveNotifierTid = WhoIs("Train Receive Notifier")) == -1) { Yield(); }
    while ((transmitNotifierTid = WhoIs("Train Transmit Notifier")) == -1) { Yield(); }
    while ((statusTid = WhoIs("Train Status Server")) == -1) { Yield(); }
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }


    long int receiveSize;
    long int receiveTid;
    char reply;

    int transmitNotifierActive = 0;
    int receiveCount = 0;
    int bank = -1; //0 indexed
    long int timeout = 100000000000;
    long int timeoutCount = 0;

    char sendBuf[TRANSMIT_BUFFER_SIZE];
    char msgBuf[MESSAGE_MAX_SIZE];

    long int sendBufSize = 0;
    long int feed = 0; 
    long int read = 0;

    Print("Timeouts:\033[37m %d", timeoutCount, 0, 0, print_timeouts); 


    for (;;) {

        receiveSize = Receive(&receiveTid, msgBuf, MESSAGE_MAX_SIZE);

        if (msgBuf[0] == 'R' && msgBuf[1] == 'e' && msgBuf[2] == 'c') {

                if (bank != -1) {
                    
                    char data = msgBuf[3];   
                    
                    msgBuf[0] = 'S';
                    msgBuf[1] = 'e';
                    msgBuf[2] = 't';
                    msgBuf[3] = 's';
                    msgBuf[4] = 'e';
                    msgBuf[5] = 'n';
                    msgBuf[6] = 's';
                             

                    for (long int i = 0; i < 8; ++i) {
                        if ((data >> i) & 1) {
                            msgBuf[7] = (bank << 4) + ((receiveCount % 2) ? 8 : 0) + (7 - i);
                            Send(statusTid, msgBuf, 8, &reply, sizeof(char));
                        }
                    }

                    --receiveCount;
                    bank = (10 - receiveCount) / 2;
                    
                    timeout = Time(clockTid) + SENSOR_TIMEOUT;
                    if (!receiveCount) timeout += 10000000000;

                }
                
                if (sendBufSize > 0 && !transmitNotifierActive && !receiveCount) {
                    Reply(transmitNotifierTid, &reply, sizeof(char));
                    transmitNotifierActive = 1;
                }

                Reply(receiveTid, &reply, sizeof(char));


        } else if (msgBuf[0] == 'T' && msgBuf[1] == 'r' && msgBuf[2] == 'a' && msgBuf[3] == 'n' && msgBuf[4] == 's') {

            if (transmitNotifierActive) {
                char cmd = sendBuf[read];
                UartWriteRegister(train_channel, UART_THR, cmd);
                read = (read + 1) % TRANSMIT_BUFFER_SIZE;    
                --sendBufSize;
                
                //if cmd = get sensor data: queue next sensor data and set expected receiveCount = 10, bank = 0
                if (cmd == 133) {
                    timeout = Time(clockTid) + FIRST_SENSOR_TIMEOUT;
                    bank = 0;
                    receiveCount = 10;

                    sendBuf[feed] = 133;
                    feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
                    ++sendBufSize;
                }
            }

            transmitNotifierActive = 0;

            if (sendBufSize > 0 && !transmitNotifierActive && !receiveCount) {
                Reply(transmitNotifierTid, &reply, sizeof(char));
                transmitNotifierActive = 1;
            }

            
        } else if (msgBuf[0] == 'c' && msgBuf[1] == 'm' && msgBuf[2] == 'd') {

            Reply(receiveTid, &reply, sizeof(char));
            
            for (long int i = 3; i < receiveSize && sendBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
                sendBuf[feed] = msgBuf[i];
                feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
                ++sendBufSize;

                //drop bytes if full
                if (sendBufSize + 1 >= TRANSMIT_BUFFER_SIZE) {
                    Print("Train Send Buffer Full", 0, 0, 0, print_log);
                    break;
                }
            }

            if (sendBufSize > 0 && !transmitNotifierActive && !receiveCount) {
                Reply(transmitNotifierTid, &reply, sizeof(char));
                transmitNotifierActive = 1;
            }


        } else if (msgBuf[0] == 'P' && msgBuf[1] == 'i' && msgBuf[2] == 'n' && msgBuf[3] == 'g') {

            Reply(receiveTid, &reply, sizeof(char));

            if (timeout < Time(clockTid)) {
                ++timeoutCount;
                Print("Timeouts:\033[37m %d", timeoutCount, 0, 0, print_timeouts); 

              //Add another sensor query to transmit queue
                timeout = Time(clockTid) + 10000000000; //will be reset when 133 is sent
                receiveCount = 0;

                if (sendBufSize == 0 || sendBuf[(feed + TRANSMIT_BUFFER_SIZE - 1) % TRANSMIT_BUFFER_SIZE] != 133) {
                    sendBuf[feed] = 133;
                    feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
                    ++sendBufSize;
                }
              
            }

            //Unblock Transmit Notifer if needed [Note: Receive Notifier never blocked here]
              if (sendBufSize > 0 && !transmitNotifierActive && !receiveCount) {
                Reply(transmitNotifierTid, &reply, sizeof(char));
                transmitNotifierActive = 1;
              }

        } else {
            Print("Invalid train server command [%.6s]", (long int)msgBuf, 0, 0, print_log);
        }
    }

}


void trainTransmitNotifier() {
    Print("Starting Train Transmit Notifier", 0, 0, 0, print_log);

    RegisterAs("Train Transmit Notifier");

    long int trainTid; 
    while ((trainTid = WhoIs("TrainRT Server")) == -1) { Yield(); }

    char reply;
    char msg[5];
    msg[0] = 'T';
    msg[1] = 'r';
    msg[2] = 'a';
    msg[3] = 'n';
    msg[4] = 's';


    for (;;) {
        Send(trainTid, msg, 5, &reply, sizeof(char));
        AwaitEvent(event_Train_Transmit);
    }

}

void trainTimeoutNotifier() {
    Print("Starting Train Timeout Notifier", 0, 0, 0, print_log);

    RegisterAs("Train Timeout Notifier");

    long int trainTid; 
    long int clockTid; 
    while ((trainTid = WhoIs("TrainRT Server")) == -1) { Yield(); }
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }

    char reply;
    char msg[4];
    msg[0] = 'P';
    msg[1] = 'i';
    msg[2] = 'n';
    msg[3] = 'g';

    for (;;) {
        Delay(clockTid, SENSOR_TIMEOUT_PING);
        Send(trainTid, msg, 4, &reply, sizeof(char));
    }

}


void trainReceiveNotifier() {
    Print("Starting Train Receive Notifier", 0, 0, 0, print_log);

    RegisterAs("Train Receive Notifier");

    long int trainTid; 
    while ((trainTid = WhoIs("TrainRT Server")) == -1) { Yield(); }
    
    char reply;
    char msg[4];
    msg[0] = 'R';
    msg[1] = 'e';
    msg[2] = 'c';

    for (;;) {
        msg[3] = TrainGetC();
        Send(trainTid, msg, 4, &reply, sizeof(char));
    }
}


