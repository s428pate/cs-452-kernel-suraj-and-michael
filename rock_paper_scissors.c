#include "rock_paper_scissors.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "common.h"
#include "rpi.h"

#define CLO 0xfe003004

/*
The RPS server should be able to support multiple pairs of clients concurrently. 
It needs to accept and provide service for the following three types of request.

    Signup      Signup requests are sent by clients that wish to play. They are 
                queued when received, and when two are on the queue the server 
                replies to each, asking for the first choice.

    Play        Play requests tell the server which of Rock, Paper or Scissors 
                the client chooses on this round. When play requests have been 
                received from a pair of clients, the server replies giving the 
                result.

    Quit        Quit tells the server that a client no longer wishes to play. 
                The server replies to let this client go, and responds to the 
                next play request from the other client by replying that the 
                other player quit. 
*/

struct rps_client {
    long int partner_tid;
    long int active;
    char move;
};

char *reply_quit = "Your partner has quit! Please Signup again.";

void rps_server() {

    Print("Starting RPS Server\n\r", 0, 0 ,0 ,0);

    RegisterAs("rps_server");

    struct rps_client clients[NUMTASKS];
    for (long int i = 0; i < NUMTASKS; ++i) {
        clients[i].partner_tid = -1;
        clients[i].active = 0;
        clients[i].move = 'X';
    }

    char msg_buf[3];
        //  Signup: "SU"
        //  Play:   "R" "P" "S"
        //  Quit:   "Q"
    long int tid;
    long int queued_tid = -1; //only 1 task can be in queue at a time
    char *reply_error = "RPS error";
    char *reply_move = "What is your first move?";
    char *reply_Win = "You played X & your opponent played Y! You Won!";
    char *reply_lost = "You played X & your opponent played Y! You Lost!";
    char *reply_tied = "You played X & your opponent played Y! You Tied!";
    char *reply_you_quit = "You have Quit!";
    //reply_quit is global


    for (;;) {

        //Receive request:
            Receive(&tid, msg_buf, 3);

        //Process request:
          //Signup: "SU"
            if (msg_buf[0] == 'S' && msg_buf[1] == 'U' && msg_buf[2] == '\0') {
                if (queued_tid != -1) {
                    clients[queued_tid].active = 1;
                    clients[queued_tid].partner_tid = tid;
                    clients[queued_tid].move = 'X';
                    clients[tid].active = 1;
                    clients[tid].partner_tid = queued_tid;
                    clients[tid].move = 'X';

                    Reply(queued_tid, reply_move, sizeof("What is your first move?"));
                    Reply(tid, reply_move, sizeof("What is your first move?"));

                    queued_tid = -1;

                } else {
                    queued_tid = tid;
                    //reply when partner found (right above)
                }
            }

          //Quit: "Q"
            else if (msg_buf[0] == 'Q' && msg_buf[1] == '\0') {
                long int temp_partner_tid = clients[tid].partner_tid;

                clients[tid].partner_tid = -1;
                clients[tid].active = 0;
                clients[tid].move = 'X';
                
                if (temp_partner_tid != -1) {
                    if (clients[temp_partner_tid].move != 'X') {
                        Reply(temp_partner_tid, reply_quit, sizeof("Your partner has quit! Please Signup again."));
                    }
                    clients[temp_partner_tid].partner_tid = -1;
                    clients[temp_partner_tid].active = 0;
                    clients[temp_partner_tid].move = 'X';

                } else {
                    queued_tid = -1;
                }

                Reply(tid, reply_you_quit, sizeof("You have Quit!"));
            }

          //Play: "R" "P" "S"
            else if ((msg_buf[0] == 'R' || msg_buf[0] == 'P' || msg_buf[0] == 'S') && msg_buf[1] == '\0') {
              //If partner hasn't quit
                if (clients[tid].active) {
                    clients[tid].move = msg_buf[0];
                    char temp_parter_move = clients[clients[tid].partner_tid].move;
                    char temp_move = clients[tid].move;
                    if (temp_parter_move != 'X') {
                        if ((temp_move == 'R' && temp_parter_move == 'S') ||
                            (temp_move == 'P' && temp_parter_move == 'R') ||
                            (temp_move == 'S' && temp_parter_move == 'P')) {
                                reply_Win[11] = temp_move;
                                reply_Win[36] = temp_parter_move;
                                reply_lost[11] = temp_parter_move;
                                reply_lost[36] = temp_move;

                                Reply(tid, reply_Win, sizeof("You played X & your opponent played Y! You Won!"));
                                Reply(clients[tid].partner_tid, reply_lost, sizeof("You played X & your opponent played Y! You Lost!"));

                                clients[tid].move = 'X';
                                clients[clients[tid].partner_tid].move = 'X';
                        } else if ((temp_move == 'S' && temp_parter_move == 'R') ||
                                   (temp_move == 'R' && temp_parter_move == 'P') ||
                                   (temp_move == 'P' && temp_parter_move == 'S')) {
                                reply_lost[11] = temp_move;
                                reply_lost[36] = temp_parter_move;
                                reply_Win[11] = temp_parter_move;
                                reply_Win[36] = temp_move;

                                Reply(tid, reply_lost, sizeof("You played X & your opponent played Y! You Lost!"));
                                Reply(clients[tid].partner_tid, reply_Win, sizeof("You played X & your opponent played Y! You Won!"));

                                clients[tid].move = 'X';
                                clients[clients[tid].partner_tid].move = 'X';
                        } else if ((temp_move == 'R' && temp_parter_move == 'R') ||
                                   (temp_move == 'P' && temp_parter_move == 'P') ||
                                   (temp_move == 'S' && temp_parter_move == 'S')) {
                                reply_tied[11] = temp_move;
                                reply_tied[36] = temp_parter_move;

                                Reply(tid, reply_tied, sizeof("You played X & your opponent played Y! You Tied!"));
                                Reply(clients[tid].partner_tid, reply_tied, sizeof("You played X & your opponent played Y! You Tied!"));

                                clients[tid].move = 'X';
                                clients[clients[tid].partner_tid].move = 'X';
                        } else {
                            Reply(tid, reply_error, sizeof("RPS error"));
                        }


                    } else {
                        //reply when partner makes move (right above)
                    }
                } else {
                    Reply(tid, reply_quit, sizeof("Your partner has quit! Please Signup again."));
                }
                                

            } else {
                Reply(tid, reply_error, sizeof("RPS error"));
            }
        


    }

}

long int rps_str_match(char *str1, char *str2, long int size) {
    for (long int i = 0; i < size; ++i) {
        if (str1[i] != str2[i]) {
            return 0;
        }
    }
    return 1;
}

void rps_client_func() {

    volatile uint32_t *clock = (uint32_t *)CLO; //source of pseudorandom

    char reply[100];
    char *signup = "SU";
    char *quit = "Q";
    char *rock = "R";
    char *paper = "P";
    char *scissors = "S";

    long int size;


  //find the RPS server by querying the name server
    long int server = WhoIs("rps_server");
    while (server == -1) { 
        server = WhoIs("rps_server");
        Yield();
    }

  //perform a set of requests that adequately test the RPS server
    for (long int i = 0; i < 2; ++i) { //Signup, Quit, signup, then finally quit
        //Signup
        Print("rps_client[%ld]:   Signing up\r\n", MyTid(), 0, 0, 0);
        size = Send(server, signup, sizeof("SU"), reply, 100);


        while ((*clock) % 5 != 0) { //20% chance to quit
            Print("\033[32mrps_server -> [%ld]:   %s\r\n\033[37m", MyTid(), (long int) reply, 0, 0);
            Print("rps_client[%ld]:     Playing ", MyTid(), 0, 0, 0);
            switch ((*clock) % 3) {
                case 0:
                    Print("Rock\r\n", 0, 0, 0, 0);
Print("\033[33mPress any key to continue.\r\n\033[37m", 0, 0, 0, 0);
uart_getc(0, 0);
                    Send(server, rock, sizeof("R"), reply, 100);
                    break;
                case 1:
                    Print("Paper\r\n", 0, 0, 0, 0);
Print("\033[33mPress any key to continue.\r\n\033[37m", 0, 0, 0, 0);
uart_getc(0, 0);
                    Send(server, paper, sizeof("P"), reply, 100);
                    break;
                case 2:
                    Print("Scissors\r\n", 0, 0, 0, 0);
Print("\033[33mPress any key to continue.\r\n\033[37m", 0, 0, 0, 0);
uart_getc(0, 0);
                    Send(server, scissors, sizeof("S"), reply, 100);
                    break;
            }


            //If Partner has quit, Signup again
            if (rps_str_match(reply, reply_quit, size)) {
                Print("\033[32mrps_server -> [%ld]:   %s\r\n\033[37m", MyTid(), (long int) reply, 0, 0);
                Print("rps_client[%ld]:   Signing up\r\n", MyTid(), 0, 0, 0);
Print("\033[33mPress any key to continue.\r\n\033[37m", 0, 0, 0, 0);
uart_getc(0, 0);
                size = Send(server, signup, sizeof("SU"), reply, 100);
            }
        }
        Print("\033[32mrps_server -> [%ld]:   %s\r\n\033[37m", MyTid(), (long int) reply, 0, 0);

        //send a quit request when they have finished playing
        Print("rps_client[%ld]:   Quiting\r\n", MyTid(), 0, 0, 0);
Print("\033[33mPress any key to continue.\r\n\033[37m", 0, 0, 0, 0);
uart_getc(0, 0);
        Send(server, quit, sizeof("Q"), reply, 100);
        Print("\033[32mrps_server -> [%ld]:   %s\r\n\033[37m", MyTid(), (long int) reply, 0, 0);
    }

  //exit gracefully
    Print("rps_client[%ld]:   Exiting\r\n", MyTid(), 0, 0, 0);
    Exit();
}


