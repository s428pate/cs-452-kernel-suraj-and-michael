***Please change names of stuff if cringe***

Output:

++---------------------------------++
++-----------------++--------------++
||Time: 000:00.00s || Idle: XX.XX% ||
++-----------------++--------------++
||  ____ _ _ __2___________        ||
||  __ _ / /  __ _ ___ _ _ \       ||
||  __ /   |/    \     /  \|       ||
||         |      \| |/    |       ||
||         |      /| |\    |       ||
|| _ _\    |\ __ /_ _ _\ _/|       ||
|| __ _\   \ _ _____1__ _ _/       ||
|| _3_ _\    \          /          ||
|| ____ _\ _ _\ ______ /_ __       ||
||                                 ||
|| 1:AA 2:BB 3:CC ...              ||
++---------------------------------++
|| Recent Sensors: AA BB CC ...    ||
++---------------------------------++
|| > [prev command]                ||
|| >                               ||
++---------------------------------++
||         [Logs/"Print"s]         ||
||                                 ||
++---------------------------------++
++---------------------------------++
turnouts:
_    _   /_  
/    \       _\   |\ \|  /| |/
straight paths:
|     _

Curves:

\  /


===========================================================================================================================
sys_call_wrapper.c:
===========================================================================================================================
Print(string, val1, val2, val3, type [log=0, ]):
  - Sends to Terminal Server IN 1 COMMAND
  - Can add formatting like location and colour based on TID = WhoIs(name);
---------------------------------------------------------------------------------------------------------------------------
Read_char?():
  - Sends signal to Terminal Server
  - Returns reply
---------------------------------------------------------------------------------------------------------------------------
Train Commands:
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    Train_speed(1 <= train <= 80, 0 <= speed <= 14):
      - Send "cmd[speed+16][train]" to Train Server
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    Train_reverse(1 <= train <= 80):
      - Create Reverse user task
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    Train_switch(1 <= turnout <= 256, direction == 'S' or 'C'):
      - Send "cmd[speed+16][train]" to Train Server
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    All Train Commands:
      - Return -1 if parameter out of range [So we can mark as red (vs. white for not formatted as a command)]
      - Take params as long ints and convert to a char/byte when sending, so we can simply return -1 if out of range
        instead of crashing
---------------------------------------------------------------------------------------------------------------------------
int Getc(int tid, int channel):
  - Send get command to tid (Train/Terminal Server)
---------------------------------------------------------------------------------------------------------------------------
int Putc(int tid, int channel, char ch):
  If tid == terminal server:
    - Print(ch->string) (which is a wrapper of send);
  Else:
    - Send "cmd[ch]" to train server
---------------------------------------------------------------------------------------------------------------------------
Print:
  - Sends to Terminal Server in 1 Send
---------------------------------------------------------------------------------------------------------------------------
===========================================================================================================================



===========================================================================================================================
terminal.c:
===========================================================================================================================
  Terminal Transmit Notifier:
    - Notifies Terminal Server when you can transmit
---------------------------------------------------------------------------------------------------------------------------
  Terminal Receive Notifier:
    - Notifies Terminal Server when you can receive
---------------------------------------------------------------------------------------------------------------------------
  Terminal Server:
    Receives Signal From:
      Terminal Transmit Notifier:
        - Sends upto tx_level amount of chars from transmit buffer
	- Suppress terminal transmit ready interrupt if buffer empty

      Terminal Receive Notifier:
        - Reads upto rx_level amount of chars while sending to tid's in receive buffer
	- Suppress terminal receive ready interrupt if receive buffer empty
     
      Print syscall wrapper:
        - Use sprintf() to write directly to buffer (watch out for edgecase where writting to end of buffer) 
        - Activate terminal transmit ready interrupt

      read_char? syscall wrapper:
        - put caller tid in receive buffer
        - Activate terminal receive ready interrupt
===========================================================================================================================
    Notes:
      - Associated interrupts suppresed by default
      - above interrupts might end up being clock timers if terminal can't send ready signal (can use C2 & C3 in that case)
===========================================================================================================================

