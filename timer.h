#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>


#define SYS_TIMER_BASE  0xFE003000

#define CS_ADDR     (SYS_TIMER_BASE)
#define CLO_ADDR    (SYS_TIMER_BASE + 0x04)
#define CHI_ADDR    (SYS_TIMER_BASE + 0x08)
#define C1_ADDR     (SYS_TIMER_BASE + 0x10)

extern volatile uint32_t *CS;
extern volatile uint32_t *CLO;
extern volatile uint32_t *CHI;
extern volatile uint32_t *C1;

extern long int time_active;
extern long int time_idle;
extern long int prev_time;

#define clock_count  (((long int)(*CHI)) << 32) + *CLO

#define msec_to_clock(msec)  (msec * 1000)

#define tick_size  msec_to_clock(10) 

#define IDLE_PRINT_INTERVAL msec_to_clock(1000)


void reset_C1();

void set_C1(uint32_t time);

void init_timer();



#endif
