#ifndef _TERMINAL_H_
#define _TERMINAL_H_

void terminalTransmitNotifier();
void terminalReceiveNotifier();
void terminalServer();

#endif