#include "handler.h"
#include "common.h"
#include "rpi.h"
#include "scheduler.h"
#include "timer.h"
#include "printf.h"
#include <stdint.h>


static long int trainCanSend = 1;

void create(long int priority, void (*function)()) {
    curr->retval = createTask(priority,function);
}

void myTid() {
    curr->retval = curr->TID;
}

void myParentTid() {
    curr->retval = curr->parentTID;
}

void yield() {
    yieldTask();
}

void exit() {
  removeTask();
}

void send(long int tid, const char *msg, long int msglen, char *reply, long int rplen) {
  kernelSend(tid, msg, msglen, reply, rplen);
}

void receive(long *tid, char *msg, long msglen) {
  kernelReceive(tid,msg,msglen);
}

void reply(long tid, const char* reply, long rplen) {
  kernelReply(tid, reply, rplen);
}

void awaitEvent(long int event){
  if (event == event_Train_Transmit && trainCanSend) {
    trainCanSend = 0;
    curr->retval = 0;
    return;
  }

  curr->retval = awaitEventTask(event);
}

void uartWriteRegister(int channel, char reg, char data) {
  uart_write_register_wrapper(channel, reg, data);
}

void uartReadRegister(int channel, char reg) {
  curr->retval = uart_read_register_wrapper(channel, reg);
}

void trainGetC() {
  awaitEvent(event_Train_Receive);
  wait_for_train_read();
}

void set_next_tick() {
  set_C1((*C1 + tick_size) % ((unsigned long int)1 << 32));
}


void interrupt_handler() {
  volatile uint32_t interrupt = *((uint32_t *)GICC_IAR);
  uint32_t interrupt_ID = interrupt & 0x3FF;

  enum events uart_interrupt_event;

  switch (interrupt_ID) {
    case CLOCK_COMPARATOR_1_IRQ:
      receiveEvent(event_Clock); 
      set_next_tick();
      reset_C1();
      break;

    case UART_IRQ:
      uart_interrupt_event = uart_interrupt_handler();

      if (uart_interrupt_event == event_Train_Transmit && !isWaitingEvent(event_Train_Transmit)) {
        trainCanSend = 1;
      }

      receiveEvent(uart_interrupt_event);
      clear_uart_interrupt();
      break;

    case 1023:
      return;

    default:
      printf("\033[33mInvalid interrupt[%ld] while running [%ld]\n\r\033[37m", interrupt_ID, curr->TID);
      break;
  }

  interrupt_handler();

  volatile uint32_t *gicc_eoir = (uint32_t *)GICC_EOIR;
  *gicc_eoir = interrupt;
}




void handle(enum request req) {
  //Need to save return values

  switch (req) {
    case req_Create:
      create(curr->p1, (void(*)(void)) curr->p2);
      break;

    case req_MyTid:
      myTid();
      break;

    case req_MyParentTid:
      myParentTid();
      break;

    case req_Yield:
      yield();
      break;

    case req_Exit:
      exit();
      break;
    
    case req_Send:
      send(curr->p1,(const char *) curr->p2, curr->p3, (char *)curr->p4, curr->p5);
      break;

    case req_Receive:
      receive((long *)curr->p1, (char *)curr->p2, curr->p3);
      break;

    case req_Reply:
      reply(curr->p1, (const char *)curr->p2, curr->p3);
      break;

    case req_Interrupt:
      interrupt_handler();
      break;

    case req_AwaitEvent:
      awaitEvent(curr->p1);
      break;
    
    case req_UartReadRegister:
      uartReadRegister((int)curr->p1, (char)curr->p2);
      break;

    case req_UartWriteRegister:
      uartWriteRegister((int)curr->p1, (char)curr->p2, (char)curr->p3);
      break;

    case req_kernel_print:
      printf((char *)curr->p1, curr->p2, curr->p3, curr->p4, curr->p5);
      break;

    case req_train_getc:
      trainGetC();
      break;

    default:
      printf("\033[33mInvalid request[%d] requested from [%ld]\n\r\033[37m", req, curr->TID);
      break;
  }
}