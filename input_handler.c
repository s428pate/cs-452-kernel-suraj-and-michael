//
// K4 [A0 implementation] user task
//
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "common.h"
#include "train_status.h"

//excludes '\0'
#define INPUT_LINE_SIZE 20 

#define TEXT_RED           "\033[31m"
#define TEXT_GREEN         "\033[32m"
#define TEXT_WHITE         "\033[37m"


char *prev_line_print_colour(int isCommand, int invalidCommand) {
  if (invalidCommand) return TEXT_RED;
  if (isCommand) return TEXT_GREEN;
  return TEXT_WHITE;
}

void input_handler() {

  Print("Starting K4 Program!", 0, 0, 0, print_log);

  Print(">", 0, 0, 0, print_prev_input);
  Print(">_", 0, 0, 0, print_input);

  Print("Valid Commands: \033[35m[\033[33mtr ## ##\033[35m|\033[33mrv ##\033[35m|\033[33msw ### ?\033[35m|\033[33mq\033[35m]  \033[35m[\033[31m#\033[35m:\033[31m0-9\033[35m|\033[31m?\033[35m:\033[31mS/C\033[35m]\n\r            Ex: [\033[33msw ### ?\033[35m] -> [\033[32msw 001 S\033[35m]    Case & Whitespace Sensitive!", 0, 0, 0, print_input_help);

  char reply;

  char line_in[INPUT_LINE_SIZE + 1];
  char prev_line_in[INPUT_LINE_SIZE + 1];
  int line_in_size = 0;

  for (int i = 0; i <= INPUT_LINE_SIZE; ++i) {
    line_in[i] = '\0';
    prev_line_in[i] = '\0';
  }

  long int terminal_tid;
  long int clock_tid;
  while ((terminal_tid = WhoIs("TerminalRT Server")) == -1) { Yield(); }
  while ((clock_tid = WhoIs("Clock Server")) == -1) { Yield(); }

  int isTrain[NUMTRAINS];
  for (int i = 0; i < NUMTRAINS; ++i) {
    isTrain[i] = 0;
  }

  for (;;) {
    //get input char
    int c = Getc(terminal_tid, terminal_channel);

    if (c == -1) {
      Print("input_handler: Wrong Server Tid[%ld]", terminal_tid, 0, 0, print_log);

    //if end of line: process
    } else if (c == '\b') {
      if (line_in_size > 0) {
        --line_in_size;
        line_in[line_in_size] = '\0';
      }

    } else if (c == '\r') {
      int isCommand = 1;
      int invalidCommand = 0;

      line_in[line_in_size] = '\0';
      ++line_in_size;

      //tr <train> <speed>
      if (line_in[0] == 't' && line_in[1] == 'r' && line_in[2] == ' ') {

        int pos = 3;
        long int train;
        long int speed;

        train = parseNum(line_in,&pos,&invalidCommand);

        if(!invalidCommand){
          pos++;
          speed =  parseNum(line_in,&pos,&invalidCommand);
        }

        if (invalidCommand || pos != line_in_size -1 || TrainSpeed(train, speed) == -1) {
          invalidCommand = 1;
        }


      //rv <train>
      } else if (line_in[0] == 'r' && line_in[1] == 'v' && line_in[2] == ' ') {
        
        long int train;
        int pos = 3;

        train = parseNum(line_in,&pos,&invalidCommand);
  

        if (invalidCommand || pos != line_in_size -1 || TrainReverse(train) == -1) {
          invalidCommand = 1;
        }


      //sw <turnout> <dir>
      } else if (line_in[0] == 's' && line_in[1] == 'w' && line_in[2] == ' ') {

        long int turnout;
        char direction;
        int pos = 3;

        turnout = parseNum(line_in,&pos,&invalidCommand);

        if(!invalidCommand){
          pos++;
          direction = line_in[pos];
          pos++;
        }
        

        if (invalidCommand || pos != line_in_size -1 || TrainSwitch(turnout, direction) == -1) {
          invalidCommand = 1;
        }
        

      //"av <train> <speed>"  [accel_val test]
      } else if (line_in[0] == 'a' && line_in[1] == 'v' && line_in[2] == ' ') {
        int pos = 3;
        long int speed = 8;
        int train;

        train = parseNum(line_in,&pos,&invalidCommand) - 1;

        if(!invalidCommand){
          pos++;
          speed = parseNum(line_in,&pos,&invalidCommand);
        }

        if (pos != line_in_size -1 || speed <= 0 || speed > 14 || train < 0 || train + 1 > NUMTRAINS) {
          invalidCommand = 1;
        }

        if(!invalidCommand) {
          Print("Train %d accel_val test w/ speed[%ld]", train + 1, speed, 0, print_log);
          accel_val_test(train, speed);
        }

 
      //"md <train> <dist_mm> <speed>"  [move_dist test]
      } else if (line_in[0] == 'm' && line_in[1] == 'd' && line_in[2] == ' ') {
        int pos = 3;
        long int dist = 0;
        long int speed = 0;
        int train;

        train = parseNum(line_in,&pos,&invalidCommand) - 1;

        if(!invalidCommand){
          pos++;
          dist = parseNum(line_in,&pos,&invalidCommand);
        }

        if(!invalidCommand){
          pos++;
          speed = parseNum(line_in,&pos,&invalidCommand);
        }

        if (pos != line_in_size -1 || dist < 0 || speed <= 0 || speed > 14 || train < 0 || train + 1 > NUMTRAINS) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {
          Print("Starting train %d move_dist test w/ dist[%ld], speed[%ld]", train + 1, dist, speed, print_log);
          Print(">%s%s", (long int)TEXT_GREEN, (long int)line_in, 0, print_prev_input);
          Print(">_", 0, 0, 0, print_input);
          move_dist_test(train, dist, speed);
        }


      //to <train> <src> <dest>
      } else if (line_in[0] == 't' && line_in[1] == 'o' 
                             && line_in[2] == ' ' 
                             ) {
         
        int pos = 3;
        long int pos1 = 0;
        long int pos2 = 0;
        int train;

        train = parseNum(line_in,&pos,&invalidCommand) - 1;

        if(!invalidCommand){
          pos++;
          pos1 = parseNum(line_in,&pos,&invalidCommand);
        }

        if(!invalidCommand){
          pos++;
          pos2 = parseNum(line_in,&pos,&invalidCommand);
        }


        if (pos != line_in_size -1 || train < 0 || train + 1 > NUMTRAINS || pos1 < 0 || pos1 > 144 || pos2 < 0 || pos2 > 144) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {
          Print("Calculating Path from %ld to %ld for Train %d", pos1, pos2, train + 1, print_log);
          dijkstras(pos1,0,pos2,0,train);
newSetup(0,train,8,0);
//printNodes(train);
          isTrain[train] = 1;

        }


      //go <speed>
      } else if (line_in[0] == 'g' && line_in[1] == 'o'&& line_in [2] == ' ') {

        int pos = 3;
        long int speed = 8;

        speed = parseNum(line_in,&pos,&invalidCommand);


        if (speed < 1 || speed > 14) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {
          if (isTrain[0] && isTrain[1]) {
            recalculate(speed);
            traverse_path(speed, 0, 0);
            traverse_path(speed, 1, 0);

          } else {
            for (int i = 0; i < NUMTRAINS; ++i) {
              if (isTrain[i]) {
                Print("Starting Traversal with train %d at speed %ld", i + 1, speed, 0, print_log);
                traverse_path(speed, i, 0);
              }
            }
          }
          
          isTrain[0] = 0;
          isTrain[1] = 0;
          
        }

      
      //col <speed>
      } else if (line_in[0] == 'c' && line_in[1] == 'o'&& line_in[2] == 'l' && line_in[3] == ' ') {

        int pos = 4;
        long int speed;

        speed = parseNum(line_in,&pos,&invalidCommand);


        if (speed < 1 || speed > 14) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {
          recalculate(speed);
        }

        
      //auto <train1 pos> <train2 pos>
      } else if (line_in[0] == 'a' && line_in[1] == 'u' && line_in[2] == 't' && line_in[3] == 'o' && line_in[4] == ' ') {
         
        int pos = 5;
        int pos1 = 0;
        int pos2 = 0;

        pos1 = parseNum(line_in,&pos,&invalidCommand);

        if(!invalidCommand){
          pos++;
          pos2 = parseNum(line_in,&pos,&invalidCommand);
        }

        if (pos != line_in_size -1 || pos1 < 0 || pos1 > 144 || pos2 < 0 || pos2 > 144) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {
          Print("Setting auto pathing with train 1 start at [%d] & train 2 start at [%d]", pos1, pos2, 0, print_log);
          long int auto_dest_tid = Create(SERVER_PRIORITY, auto_dest);
          Send(auto_dest_tid, (char *)&pos1, sizeof(int), &reply, sizeof(char));
          Send(auto_dest_tid, (char *)&pos2, sizeof(int), &reply, sizeof(char));
        }
        

      //pp <train> (print path)
      } else if (line_in[0] == 'p' && line_in[1] == 'p') {
        
        int pos = 3;
        int train;
        train = parseNum(line_in,&pos,&invalidCommand) - 1;

        if (train < 0 || train > NUMTRAINS - 1) {
          invalidCommand = 1;
        }

        if (!invalidCommand) {   
          printNodes(train);
        }


      //q
      } else if (line_in_size == 2 && line_in[0] == 'q') {
        Print(">%sq", (long int)TEXT_GREEN, 0, 0, print_prev_input);
        Print(">_", 0, 0, 0, print_input);
        Print("%sSystem Rebooting...", (long int)TEXT_RED, 0, 0, print_log);
        for (long int i = 1; i <= 80; ++i) {
          pure_train_reverse(i);
          TrainSpeed(i, 0);
        }
        Delay(clock_tid, 5 * 80);
        Shutdown();
        

      //Not a command
      } else {
        isCommand = 0;
      }

      for (int i = 0; i < line_in_size; ++i) {
        prev_line_in[i] = line_in[i];
      }

      line_in_size = 0;
      for (int i = 0; i <= INPUT_LINE_SIZE; ++i) {
        line_in[i] = '\0';
      }

      Print(">%s%s", (long int)prev_line_print_colour(isCommand, invalidCommand), (long int)prev_line_in, 0, print_prev_input);

    } else {
      line_in[line_in_size] = c;
      ++line_in_size;
    }

    if (line_in_size == INPUT_LINE_SIZE) {

      line_in[line_in_size] = '\0';
      ++line_in_size;

      for (int i = 0; i < line_in_size; ++i) {
        prev_line_in[i] = line_in[i];
      }

      line_in_size = 0;
      for (int i = 0; i <= INPUT_LINE_SIZE; ++i) {
        line_in[i] = '\0';
      }

      Print(">%s", (long int)prev_line_in, 0, 0, print_prev_input);
    }

    Print(">%s_", (long int)line_in, 0, 0, print_input);
  }
}
