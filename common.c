#include "common.h"
#include "sys_call_wrapper.h"

struct task_descriptor *curr;
int waiting;

int parseNum(char *str, int* pos, int* invalid){
    int sign = 1;
    
    if(str[*pos] == '-'){ 
        sign = -1; 
        (*pos)++;
    }
    if(str[*pos] < '0' || str[*pos] > '9'){
        *invalid = 1;
        return 0; 
    }
    int num = 0; 
    while(str[*pos] >= '0' && str[*pos] <= '9'){
        num *= 10;
        num += str[*pos] - '0';
        (*pos)++;
    }
    return num * sign;
}

// Credit to https://en.wikipedia.org/wiki/Integer_square_root#Digit-by-digit_algorithm: [It's the Python code there which I converted to C]
long int sqrt(long int n) {
    if (n < 0) {
        Print("sqrt() works for only non-negative integers!", 0, 0, 0, print_log);
        return -1;
    }
    
    if (n < 2) { 
        return n; 
    }

    // Find the shift amount. See also [[find first set]],
    // shift = ceil(log2(n) * 0.5) * 2 = ceil(ffs(n) * 0.5) * 2
    long int shift = 2;
    while ((n >> shift) != 0) {
        shift += 2;
    }

    // Unroll the bit-setting loop.
    long int result = 0;
    long int large_cand;
    while (shift >= 0) {
        result = result << 1;
        large_cand = result ^ 1;
        //# Same as result ^ 1 (xor), because the last bit is always 0.
        if (large_cand * large_cand <= n >> shift) {
            result = large_cand;
        }
        shift -= 2;
    }

    return result;
}