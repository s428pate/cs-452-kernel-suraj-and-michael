#include "scheduler.h"
#include "common.h"
#include "rpi.h"
#include "printf.h"



#define SPSR_VALUE 0

struct task_descriptor taskBlock[NUMTASKS];


long int count;
struct task_descriptor *front[PRIORITIES+4]; //4 extra for SENDWAIT, REPLYWAIT, RECEIVEWAIT, INTERRUPTWAIT
struct task_descriptor *back[PRIORITIES+4];


void removeFromQueue(int priority, struct task_descriptor *td){
    
    if(front[priority] == td){
        front[priority] = td->next;
    }
    if(back[priority] == td){
        back[priority] = td->prev;
    }
    if(td->prev){
        td->prev->next = td->next;
    }
    if(td->next){
        td->next->prev = td->prev;
    }
    
        
}

void addToQueue(long int priority, struct task_descriptor *td){
    td->prev = back[priority];
    td->next = NULL;
    if(!front[priority]){
        front[priority] = td;
        back[priority] = td;
    }else{
        back[priority]->next = td;
        back[priority] = td;
    }
}

void kernelSend(long int tid, const char *msg, long int msglen, char *reply, long int rplen){

    if(tid >= count ||!taskBlock[tid].active){
        curr->retval = -1;
        printf("\033[33mTask sent to invalid task!\n\r\033[37m");
        return;
    }

    if(tid == curr->TID) {
        curr->retval = -2;
        printf("\033[33mTask sent to self!\n\r\033[37m");
        return;
    }

    curr->sendTid = tid;
    curr->replyBuff = reply;

    for(int i = 0; i< msglen;++i){
        curr->sendBuff[i] = msg[i];
    }

    curr->sendSz = msglen;
    curr->replySz = rplen;

    if(taskBlock[tid].stat == (enum status) stat_ReceiveWait){
       
        for(int i = 0; i < taskBlock[tid].receiveSz && i < msglen;++i){
            taskBlock[tid].receiveBuff[i] = msg[i];
        }
        taskBlock[tid].retval = msglen;
        *(taskBlock[tid].receiveTid) = curr->TID;

        struct task_descriptor *temp = front[PRIORITIES + (enum status) stat_ReceiveWait];

        while(temp->TID != tid){
            if(!temp){
                printf("\033[33msomething is very wrong\n\r\033[37m");
            }
            temp = temp->next;
        }

        removeFromQueue(PRIORITIES + (enum status) stat_ReceiveWait,temp);

        temp->retval = msglen;
        *(temp->receiveTid) = curr->TID;
        temp->stat = (enum status)stat_Ready;

        addToQueue(temp->priority,temp);

        //remove from priority queue
        removeFromQueue(curr->priority,curr);

        curr->stat = (enum status) stat_ReplyWait;
        //add to replywait queue
        addToQueue(PRIORITIES + (enum status) stat_ReplyWait, curr);

        

    }else{

         //remove from priority queue
         removeFromQueue(curr->priority,curr);

        curr->stat = (enum status) stat_SendWait;
        //add to sendwait queue

        addToQueue(PRIORITIES + (enum status) stat_SendWait, curr);
     
        curr = schedule();
    }


    return;
}

void kernelReceive(long int *tid, char *msg, long int msglen){
    
    struct task_descriptor *temp = front[PRIORITIES + (enum status) stat_SendWait];
    curr->receiveTid = tid;
    curr->receiveSz = msglen;

    while(temp){
        if(temp->sendTid == curr->TID){
            
            for(int i = 0; i < temp->sendSz && i < msglen;++i){
                msg[i] = temp->sendBuff[i];
            }

            curr->retval = temp->sendSz;
            *curr->receiveTid = temp->TID;
            *tid = temp->TID;

            removeFromQueue(PRIORITIES + (enum status) stat_SendWait,temp);

            temp->stat = (enum status) stat_ReplyWait;

            addToQueue(PRIORITIES + (enum status) stat_ReplyWait,temp);
            
            //done current thing

            curr->retval = temp->sendSz;
            return;
        }

        temp = temp->next;
    }
    
    curr->receiveBuff = msg;

    //remove from priority queue
    removeFromQueue(curr->priority,curr);\

    curr->stat = (enum status) stat_ReceiveWait;
    addToQueue(PRIORITIES + (enum status) stat_ReceiveWait, curr);

    

    //curr = schedule()?

    return;
}

void kernelReply(long int tid, const char *reply, long int rplen){
    if(tid >= count ||!taskBlock[tid].active){
        curr->retval = -1;
        return;
    }
    if(taskBlock[tid].stat != (enum status)stat_ReplyWait){
        curr->retval = -2;
        return;
    }

    long int i;
    for( i = 0; i < rplen && i < taskBlock[tid].replySz;++i){
        taskBlock[tid].replyBuff[i] = reply[i];
    }

    taskBlock[tid].stat = (enum status)stat_Ready;
    taskBlock[tid].retval = rplen;

    removeFromQueue(PRIORITIES + (enum status) stat_ReplyWait,&taskBlock[tid]);
    addToQueue(taskBlock[tid].priority,&taskBlock[tid]);

    curr->retval = i;
    yieldTask(); 
    return;
    
}

void setupScheduler(){
    for (int i = 0; i < PRIORITIES + 4; i++){
        front[i] = NULL;
        back[i] = NULL;
    }
    count = 0;

    curr = NULL;
}

long int createTask(long int priority, void(*function)()){
    if(priority > PRIORITIES -1 || priority < 0){
        printf("\033[33mreturning -1 (bad priority: %ld)\n\r\033[37m",priority);
        return -1;
    }
    int first_free = count;
    if (count >= NUMTASKS){
        for(int i = 0; i < NUMTASKS; ++i){
            if(!taskBlock[i].active){
                first_free = i;
                break;
            }
        }
        if(first_free == count){
            printf("\033[33m    returning -2 oops (full), count = %ld\n\r\033[37m",count);
            return -2;
        }
        
    }

    struct task_descriptor * t = &taskBlock[first_free];
    
    t->TID = first_free;
    t->sp = t->mystack + STACK_SIZE - 8 * 50; //should be enough space for initial stack_pops
    t->spsr = SPSR_VALUE;
    t->retval = 0;
    t->p1 = 0;
    t->p2 = 0;
    t->p3 = 0;
    t->p4 = 0;
    t->p5 = 0;
    t->priority = priority;
    t->pc = function;
    t->next = NULL;
    t->prev = NULL;
    t->active = 1;
    t->stat = (enum status)stat_Ready;

    if(curr){
        t->parentTID = curr->TID;
    }else{
        t->parentTID = -1;
    }
    

    addToQueue(priority,t);

    count++;
    return t->TID;
}


struct task_descriptor* schedule(){
    for(int i = PRIORITIES-1; i > 0; i--){
       if(front[i]!= NULL){
        return front[i];
       }
    }
    for(int i = PRIORITIES; i < PRIORITIES +4; ++i){
        if(front[i]!= NULL){
        waiting = 1;
        return front[0];
       }
    }
    waiting = 0;
    return front[0]; //no unblocked tasks   
}

void removeTask(){
    
    struct task_descriptor *temp = front[PRIORITIES + (enum status) stat_SendWait];

    //cleaning up
    while(temp){
        if(temp->sendTid == curr->TID){

            removeFromQueue(PRIORITIES + (enum status) stat_SendWait,temp);

            temp->stat = (enum status) stat_Ready;

            addToQueue(temp->priority,temp);
            
            temp->retval = -1;
        }
        temp = temp->next;
    }
    
   
    front[curr->priority] = front[curr->priority]->next;
    
    if(back[curr->priority] == curr){
        back[curr->priority] = NULL;
    }else{
        front[curr->priority]->prev = NULL;
    }
    curr->active = 0;
    curr = schedule(); 
}



void yieldTask(){
    if(curr->next != NULL){
        front[curr->priority] = curr->next;
        front[curr->priority]->prev = NULL;
        back[curr->priority]->next = curr;
        curr->prev = back[curr->priority];
        back[curr->priority] = curr;
        curr->next = NULL;
        
    } //otherwise this is the only element in the priority so we keep it as is

}

long int awaitEventTask(long int event){
    if(event < event_Clock || event_none < event) {
        printf("\033[33mTried to await invalid event [%ld]\n\r\033[37m", event);
        return -1;
    }

    removeFromQueue(curr->priority,curr);
    curr->stat = (enum status) stat_EventWait;
    curr->awaitingEvent = event;
    addToQueue(PRIORITIES + (enum status) stat_EventWait,curr);

    switch (event) {
        case event_Terminal_Receive:
        case event_Terminal_Transmit:
            enable_uart_interrupt(event);
            break;

        case event_Clock:
        case event_Train_Receive:
        case event_Train_Transmit:
        case event_none:
            break;
        
        default:
            printf("Event [%ld] not added to awaitEventTask uart interrupt switch!\n\r", event);
            break;
    }
    return 0;
}

long int receiveEvent(long int event){
    if(event < event_Clock || event_none < event) {
        printf("\033[33mTried to receive invalid event [%ld]\n\r\033[37m", event);
        return -1;
    }
    
    struct task_descriptor *temp = front[PRIORITIES + (enum status) stat_EventWait];
    while(temp){
        if(temp->awaitingEvent == event){
            struct task_descriptor *temp2 = temp->next;
            temp->stat = (enum status) stat_Ready;
            removeFromQueue(PRIORITIES + (enum status) stat_EventWait,temp);
            addToQueue(temp->priority,temp);
            temp = temp2;
        }else{
            temp = temp->next;
        }
    }

    switch (event) {
        case event_Terminal_Receive:
        case event_Terminal_Transmit:
            disable_uart_interrupt(event);
            break;

        case event_Clock:
        case event_Train_Receive:
        case event_Train_Transmit:
        case event_none:
            break;
            
        default:
            printf("Event [%ld] not added to receiveEvent uart interrupt switch!\n\r", event);
            break;
    }
    return 0;
}

long int isWaitingEvent(long int event) {

    if (event < event_Clock || event_none < event) {
        printf("\033[33mTried to check if waiting for invalid event [%ld]\n\r\033[37m", event);
        return 0;
    }

    struct task_descriptor *temp = front[PRIORITIES + (enum status) stat_EventWait];
    while (temp) {
        if (temp->awaitingEvent == event) {
           return 1;
        }    
        temp = temp->next;
    }

    return 0;
}
