#ifndef _TRAIN_SWITCH_SERVER_H_
#define _TRAIN_SWITCH_SERVER_H_


void train_switch_server();

void train_switch_notifier();

void set_train_switch(char turnout, char direction_byte);


#endif