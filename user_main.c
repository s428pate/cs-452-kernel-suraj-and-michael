#include "user_main.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "common.h"
#include "timer.h"
#include "name_server.h"
#include "clock_server.h"
#include "rock_paper_scissors.h"
#include "track_data_new.h"
#include "train_status.h"

// Priority allocation
//    Priority 5: Servers
//    Priority 1-4: Free to use
//    Priority 0: Idle Task

extern void wait_for_interrupt();

void idle_task() {
  Print("Starting Idle Task for first time", 0, 0, 0, print_log);

  long int last_print = msec_to_clock(-10000);

  for (;;) {
    time_active += clock_count - prev_time;
    prev_time = clock_count;

    wait_for_interrupt();

    time_idle += clock_count - prev_time;
    prev_time = clock_count;

    if (last_print + IDLE_PRINT_INTERVAL < clock_count) {
      last_print = clock_count;
      Print("System Idle for: \033[37m%.2ld.%.3ld%%", 100 * time_idle / (time_active + time_idle), ( (time_idle * 100 * 1000) / (time_active + time_idle) ) % 1000, 0, print_idle);
      time_active = 0;
      time_idle = 0;
    }
  }
  Print("Error: Idle Task Reached End!", 0, 0, 0, print_log);
  Exit();
}

void clock_display() {
  Print("Starting Clock Display\n\r", 0, 0, 0, print_log);

  long int tid;
  while ((tid = WhoIs("Clock Server")) == -1) {}
  
  long int time_tenth_sec = 0;

  for (;;) {
    Print("Time: %ld:%.2ld.%ld0", time_tenth_sec / 600, (time_tenth_sec / 10) % 60, time_tenth_sec % 10, print_time);
    Delay(tid, 10);
    ++time_tenth_sec;
  }
}

void init_train() {
  Print("", 0, 0, 0, print_seperators);
  Print("System Idle for: \033[37m00.000%%", 0, 0, 0, print_idle);
  Print("Initializing Track...", 0, 0, 0, print_log);
  Print("(Track initialisation will take quite a few seconds)", 0, 0, 0, print_log);

#ifdef IS_TRACKA
  init_tracka(mytrack);
#else
  init_trackb(mytrack);
#endif

  long int tid;
  while ((tid = WhoIs("Clock Server")) == -1) { Yield(); }

  for (long int i = 1; i <= 80; ++i) {
    pure_train_reverse(i);
    TrainSpeed(i, 0);
  }

  Delay(tid, 80 * 5); //since 100-500ms delay occurs only under assumption of not full buffer

  //set switches/turnouts
  for (long int i = 1; i <= 18; ++i) {
    switch (i) {
      case 6:
      case 7:
      case 8:
      case 13:
        TrainSwitch(i, 'S');
        break;
      default:
        TrainSwitch(i, 'C');
        break;
    }
  }

  TrainSwitch(154, 'C');
  
#ifdef IS_TRACKA
  TrainSwitch(156, 'C');
#else
  TrainSwitch(156, 'S');
#endif

  Delay(tid, SWITCH_DELAY * 25);

  long int trainRT_server_tid;
  while ((trainRT_server_tid = WhoIs("TrainRT Server")) == -1) { Yield(); }  

  Putc(trainRT_server_tid, train_channel, 192); //reset mode for sensors
  Putc(trainRT_server_tid, train_channel, 133); //first sensor query
  
  Print("Initialized Track!", 0, 0, 0, print_log);

  Exit();
}


//Testing [To Reuse]:

void quick_test() {
  //general function for quick tests so we don't have to keep making new functions

  Print("Starting Quick Test", 0, 0, 0, print_log);
  
  long int tid;
  while ((tid = WhoIs("Clock Server")) == -1) { Yield(); }

  init_trackb(mytrack);
  Print("Done Initializing track",0,0,0,print_log);

  for (;;) {
    dijkstras(12,0,28,0,0);
    Print("Starting setup",0,0,0,print_log);
    newSetup(0,0,10,0);
    Print("Done setup",0,0,0,print_log);
    AwaitEvent(event_Terminal_Receive);
    for(int i = 0; i<pathLen[0]; ++i ){
      Print("Node [%ld], startTime [%ld], endtime [%ld]",i,path[0][i]->TrainStartTime[0],path[0][i]->TrainEndTime[0],print_log);
    }

    Print("Done printing",0,0,0,print_log);
    AwaitEvent(event_Terminal_Receive);
  }
  
  
  Exit();
}


//Testing [To Remove Later]:

void dijkTest(){
  Print("Actually Initializing track",0,0,0,print_log);
  init_trackb(mytrack);
  Print("Done Initializing track",0,0,0,print_log);
  dijkstras(1,0,44,0,0);
  Print("Done dijkstras",0,0,0,print_log);
  Exit();
}
