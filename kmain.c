#include "initialize.h"
#include "scheduler.h"
#include "activate.h"
#include "handler.h"
#include "common.h"
#include "printf.h"
#include "rpi.h"
#include "user_main.h"

#include "timer.h"
#include <stdint.h>

void kmain() {

  enum request req;

  initialize();

  time_active = 0;
  time_idle = 0;

  prev_time = clock_count;

  for (;;) {

    curr = schedule();

    //if no blocked tasks and Idle Task scheduled, end kernel
    if (curr->priority == IDLE_TASK_PRIORITY && !waiting) {
      time_active += clock_count - prev_time;
      prev_time = clock_count;
      break;
    }

    req = activate(curr);

    if (req == req_Shutdown) break;
    //printf("handling [%d] from [%ld]\n\r", req, curr->TID);
    handle(req);
  }
  
}
