#include "terminal.h"
#include "common.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"


#define LOG_REGION  "\033[31;67r"
#define CURSOR_LOG  "\033[66;1H"

#define CURSOR_HIDE "\033[?25l"
#define SCREEN_CLEAR "\033[2J"


void terminalServer() {
    KernelPrint("Starting Terminal Server\n\r", 0, 0, 0, 0);

    long int receiveNotifierTid; 
    long int transmitNotifierTid; 
    while ((receiveNotifierTid = WhoIs("Terminal Receive Notifier")) == -1) { Yield(); }
    while ((transmitNotifierTid = WhoIs("Terminal Transmit Notifier")) == -1) { Yield(); }


    long int receiveSize;
    long int receiveTid;
    char reply;
    long int getCTid = -1;
    int receiveNotifierActive = 0;
    int transmitNotifierActive = 0;

    char printBuf[TRANSMIT_BUFFER_SIZE];
    char msgBuf[MESSAGE_MAX_SIZE];

    long int printBufSize = 0;
    long int feed = 0; 
    long int read = 0;
    

    for (unsigned long int i = 0; i < sizeof(SCREEN_CLEAR) - 1 && printBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
        printBuf[feed] = SCREEN_CLEAR[i];
        feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
        ++printBufSize;
    }
    
    for (unsigned long int i = 0; i < sizeof(CURSOR_HIDE) - 1 && printBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
        printBuf[feed] = CURSOR_HIDE[i];
        feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
        ++printBufSize;
    }

    for (unsigned long int i = 0; i < sizeof(LOG_REGION) - 1 && printBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
        printBuf[feed] = LOG_REGION[i];
        feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
        ++printBufSize;
    }

    for (unsigned long int i = 0; i < sizeof(CURSOR_LOG) - 1 && printBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
        printBuf[feed] = CURSOR_LOG[i];
        feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
        ++printBufSize;
    }
    
    RegisterAs("TerminalRT Server");

    for (;;) {

        receiveSize = Receive(&receiveTid, msgBuf, MESSAGE_MAX_SIZE);

        if (msgBuf[0] == 'R' && msgBuf[1] == 'e' && msgBuf[2] == 'c') {
            if (receiveNotifierActive) {
                reply = UartReadRegister(terminal_channel, UART_RHR);
                Reply(getCTid, &reply, sizeof(char));
                getCTid = -1;
            }
            
            receiveNotifierActive = 0;


        } else if (msgBuf[0] == 'T' && msgBuf[1] == 'r' && msgBuf[2] == 'a' && msgBuf[3] == 'n' && msgBuf[4] == 's') {
            
            if (transmitNotifierActive) {
                UartWriteRegister(terminal_channel, UART_THR, printBuf[read]);
                read = (read + 1) % TRANSMIT_BUFFER_SIZE;    
                --printBufSize;
            }

            transmitNotifierActive = 0;

            if (printBufSize > 0) {
                Reply(transmitNotifierTid, &reply, sizeof(char));
                transmitNotifierActive = 1;
            }

            
        } else if (msgBuf[0] == 'P' && msgBuf[1] == 'r' && msgBuf[2] == 'i' && msgBuf[3] == 'n' && msgBuf[4] == 't') {

            Reply(receiveTid, &reply, sizeof(char));
            
            for (long int i = 5; i < receiveSize && printBufSize + 1 < TRANSMIT_BUFFER_SIZE; ++i) { 
                printBuf[feed] = msgBuf[i];
                feed = (feed + 1) % TRANSMIT_BUFFER_SIZE;
                ++printBufSize;

                //drop chars if full
                if (printBufSize + 1 >= TRANSMIT_BUFFER_SIZE) {
                    KernelPrint("Print Buffer Full\n\r", 0, 0, 0, 0);
                    break;
                }
            }

            if (printBufSize > 0 && !transmitNotifierActive) {
                Reply(transmitNotifierTid, &reply, sizeof(char));
                transmitNotifierActive = 1;
            }



        } else if (msgBuf[0] == 'G' && msgBuf[1] == 'e' && msgBuf[2] == 't' && msgBuf[3] == 'C') {
            
            if (receiveNotifierActive) {
                KernelPrint("2 GetC's at once!\n\r", 0, 0, 0, 0);

            } else {
                getCTid = receiveTid;

                Reply(receiveNotifierTid, &reply, sizeof(char));
                receiveNotifierActive = 1;
            }


        } else {
            KernelPrint("Invalid terminal server command [%.6s]", (long int)msgBuf, 0, 0, 0);
        }
    }

}


void terminalTransmitNotifier(){
    KernelPrint("Starting Terminal Transmit Notifier\n\r", 0, 0, 0, 0);

    RegisterAs("Terminal Transmit Notifier");

    long int terminalTid; 
    while ((terminalTid = WhoIs("TerminalRT Server")) == -1) { Yield(); }

    char reply;
    char msg[5];
    msg[0] = 'T';
    msg[1] = 'r';
    msg[2] = 'a';
    msg[3] = 'n';
    msg[4] = 's';

    for (;;) {
        Send(terminalTid, msg, 5, &reply, sizeof(char));
        AwaitEvent(event_Terminal_Transmit);
    }

}
void terminalReceiveNotifier(){
    KernelPrint("Starting Terminal Receive Notifier\n\r", 0, 0, 0, 0);

    RegisterAs("Terminal Receive Notifier");

    long int terminalTid; 
    while ((terminalTid = WhoIs("TerminalRT Server")) == -1) { Yield(); }

    char reply;
    char msg[3];
    msg[0] = 'R';
    msg[1] = 'e';
    msg[2] = 'c';

    for (;;) {
        Send(terminalTid, msg, 3, &reply, sizeof(char));
        AwaitEvent(event_Terminal_Receive);
    }
}
