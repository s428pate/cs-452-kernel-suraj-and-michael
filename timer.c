#include "timer.h"
#include <stdint.h>


volatile uint32_t *CS;
volatile uint32_t *CLO;
volatile uint32_t *CHI;
volatile uint32_t *C1;

long int time_active;
long int time_idle;
long int prev_time;

void reset_C1() {
    *CS |= 0x2;
}

void set_C1(uint32_t time) {
    *C1 = time;
}

void init_timer() {

    CS  = (uint32_t *) CS_ADDR;
    CLO = (uint32_t *) CLO_ADDR;
    CHI = (uint32_t *) CHI_ADDR;
    C1  = (uint32_t *) C1_ADDR;

    //Setup First Tick
    set_C1((clock_count + tick_size) % ((unsigned long int)1 << 32));
}
