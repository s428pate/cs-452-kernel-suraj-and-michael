#include "initialize.h"
#include "scheduler.h"
#include "printf.h"
#include "rpi.h"
#include <stdint.h>
#include "common.h"
#include "timer.h"
#include "user_main.h"
#include "clock_server.h"
#include "name_server.h"
#include "terminal.h"
#include "train.h"
#include"train_status.h"
#include "input_handler.h"
#include "train_switch_server.h"


void enable_interrupt(uint32_t interrupt_ID) {

  volatile uint32_t *gicd_isenabler = (uint32_t *) GICD_ISENABLER(interrupt_ID);
  volatile uint32_t *gicd_itargetsr = (uint32_t *) GICD_ITARGETSR(interrupt_ID);


  *gicd_isenabler |= GICD_ISENABLER_value(interrupt_ID);
  *gicd_itargetsr |= GICD_ITARGETSR_CPU_0(interrupt_ID);

} 

void init_GIC_interrupts() {

  //Enable GIC interrupts
  enable_interrupt(CLOCK_COMPARATOR_1_IRQ); //clock server ticks
  enable_interrupt(UART_IRQ);
  
}

extern void init_exception_vector();


void initialize() {
  
  //Setup device stuff
  init_gpio();
  init_spi(0);
  init_uart(0);

  //caching:
  //SCTLR_EL1 

  printf("\033[33mInitializing Suraj & Michael's Kernel!\n\r");

  printf("   Initializing Scheduler\n\r");
  setupScheduler();

  printf("   Initializing Tasks\n\r");
    printf("      Creating Servers:\n\r");
      printf("         Name[");
        printf("%ld]-", createTask(SERVER_PRIORITY, name_server));
      printf("Clock[");
        printf("%ld]-", createTask(SERVER_PRIORITY, clock_server));
      printf("terminal[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, terminalServer));
      printf("train [");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, trainServer));
      printf("train status[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, trainStatusServer));
      printf("train switch[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, train_switch_server));
      
    printf("      Creating Notifiers:\n\r");
      printf("         Clock[");
        printf("%ld]-", createTask(SERVER_PRIORITY, clock_notifier));
      printf("terminal_rec[");
        printf("%ld]-", createTask(SERVER_PRIORITY, terminalReceiveNotifier));
      printf("terminal_trans[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, terminalTransmitNotifier));
        printf("train_rec[");
        printf("%ld]-", createTask(SERVER_PRIORITY, trainReceiveNotifier));
      printf("train_trans[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, trainTransmitNotifier));
      printf("train_timeout[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, trainTimeoutNotifier));
      printf("train_switch[");
        printf("%ld]\n\r", createTask(SERVER_PRIORITY, train_switch_notifier));
    printf("      Creating Idle Task [");
      printf("%ld]\n\r", createTask(IDLE_TASK_PRIORITY, idle_task));
      printf("      Creating User Tasks:\n\r");
        printf("init_train[");
          printf("%ld]-", createTask(4, init_train));
        printf("switch_setter[");
          printf("%ld]-", createTask(2, train_switch_setter));
        printf("input_handler[");
#ifdef DEBUG_LOGS
          printf("%ld]\n\r", createTask(3, input_handler));
#else
          printf("%ld]\n\r", createTask(SERVER_PRIORITY, input_handler));
#endif
  
  printf("   Initializing exception vector\n\r");
  init_exception_vector();

  printf("   Initializing Interrupts\n\r");
  printf("      GIC-");
  init_GIC_interrupts();
  printf("GPIO\n\r");
  init_gpio_interrupts();

  printf("   Initializing timer (C1 initialised on program start)\n\r");
  init_timer();

  printf("   Initialized Suraj & Michael's Kernel!\n\r");

}

