#include "clock_server.h"
#include "stdint.h"
#include "common.h"
#include "sys_call.h"
#include "sys_call_wrapper.h"
#include "scheduler.h"

void clock_server(){

    Print("Starting Clock Server", 0, 0, 0, print_log);

    uint32_t waitingTicks[NUMTASKS];
    char msg_buf[MESSAGE_MAX_SIZE];
    

    long int tid;
    uint32_t reply;
    uint32_t ticks = 0;
    uint32_t smallestDelay = UINT32_MAX;
    long int pos = -1;

    for (long int i = 0; i < NUMTASKS; ++i) {
        waitingTicks[i] = UINT32_MAX;
    }

    RegisterAs("Clock Server");

    for(;;){
        Receive(&tid, msg_buf, MESSAGE_MAX_SIZE);

        //Process request:
        
        //Time()
        if (msg_buf[0] == 'T' && msg_buf[1] == 'i' && msg_buf[2] == 'm' && msg_buf[3] == 'e') { //rip rip Tim
            reply = ticks;
            Reply(tid, (char *)&reply, sizeof(reply));
        } 
            
        //DelayUntil()
        else if (msg_buf[0] == 'D' && msg_buf[1] == 'e' && msg_buf[2] == 'l' && msg_buf[3] == 'U') {
            for (long unsigned int i = 0; i < sizeof(int); ++i) {
                *((char *)(waitingTicks + tid) + i) = msg_buf[4 + i];    
            }

            if (waitingTicks[tid] <= ticks) {
                waitingTicks[tid] = UINT32_MAX;
                reply = ticks;
                Reply(tid, (char *)&reply, sizeof(reply));
            }
            else if (waitingTicks[tid] < smallestDelay) {
                smallestDelay = waitingTicks[tid];
                pos = tid;
            }
        }
        //Delay()
        else if (msg_buf[0] == 'D' && msg_buf[1] == 'e' && msg_buf[2] == 'l') {
            for (long unsigned int i = 0; i < sizeof(int); ++i) {
                *((char *)(waitingTicks + tid) + i) = msg_buf[4 + i];
            }
            
            waitingTicks[tid] += ticks;
            if (waitingTicks[tid] == ticks) {
                waitingTicks[tid] = UINT32_MAX;
                reply = ticks;
                Reply(tid, (char *)&reply, sizeof(reply));

            }
            else if (waitingTicks[tid] < smallestDelay) {
                smallestDelay = waitingTicks[tid];
                pos = tid;
            }
            
        //Tick()
        } else if (msg_buf[0] == 'T' && msg_buf[1] == 'i' && msg_buf[2] == 'c' && msg_buf[3] == 'k') {
            ticks++;
            reply = 0; //can be anything
            Reply(tid, (char *)&reply, sizeof(reply));
            while (waitingTicks[pos]  <= ticks) {
                reply = ticks;
                Reply(pos, (char *)&reply, sizeof(reply));
                waitingTicks[pos] = UINT32_MAX;
                smallestDelay = UINT32_MAX;

                //find next smallest
                for (int i = 0; i < NUMTASKS; ++i) {
                    if(waitingTicks[i] < smallestDelay){
                        smallestDelay = waitingTicks[i];
                        pos = i;
                    }
                }
            }

            if (ticks % 10 == 0) {
                long int time_tenth_sec = ticks / 10;
                Print("Time: \033[37m%02ld:%.2ld.%.1ld0", time_tenth_sec / 600, (time_tenth_sec / 10) % 60, time_tenth_sec % 10, print_time);
            }
                
        }else{
            Print("Invalid Clock server command [%.6s]",(long int)msg_buf,0,0,0);
        }
    }
}

long int Tick (long int tid) {

    char msg[MESSAGE_MAX_SIZE];
    msg[0] = 'T';
    msg[1] = 'i';
    msg[2] = 'c';
    msg[3] = 'k';

    long int reply;

    if (Send(tid, msg, 4, (char *) &reply, sizeof(reply)) < 0) return -1;
    
    return reply; 

}

void clock_notifier() {

    Print("Starting Clock Notifier", 0, 0, 0, print_log);

    long int clockTid; 
    while ((clockTid = WhoIs("Clock Server")) == -1) { Yield(); }
    
    for (;;) {
        AwaitEvent(event_Clock);
        Tick(clockTid);
    }
}
