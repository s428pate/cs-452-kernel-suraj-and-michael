#ifndef _TRAIN_H_
#define _TRAIN_H_

void trainTransmitNotifier();
void trainReceiveNotifier();
void trainTimeoutNotifier();
void trainServer();

#endif