#ifndef _RPI_H_
#define _RPI_H_

#include <stdint.h>
#include <stddef.h>
#include "common.h"


#define GIC_BASE                  (long int)0xFF840000
#define GICD_OFFSET               0x1000
#define GICC_OFFSET               0x2000
#define GICD_BASE                 (GIC_BASE + GICD_OFFSET)
#define GICC_BASE                 (GIC_BASE + GICC_OFFSET)

#define GICD_CTLR                 GICD_BASE
#define GICC_IAR                  (GICC_BASE + 0x000c)
#define GICC_EOIR                 (GICC_BASE + 0x0010)

#define GICD_ISENABLER(x)         (GICD_BASE + 0x100 + (0x4 * (x / 32)))
#define GICD_ISENABLER_value(x)   (uint32_t)(1 << (x % 32))
#define GICD_ITARGETSR(x)         (GICD_BASE + 0x800 + (0x4 * (x / 4)))
#define GICD_ITARGETSR_CPU_0(x)   (1 << (8 * (x % 4)))

#define VIDEO_CORE_BASE           96
#define CLOCK_COMPARATOR_1_IRQ    (VIDEO_CORE_BASE + 1)
#define UART_IRQ                  (VIDEO_CORE_BASE + 49)


void init_gpio();
void init_spi(uint32_t channel);
void init_uart(uint32_t spiChannel);
char uart_getc(size_t spiChannel, size_t uartChannel);
void uart_putc(size_t spiChannel, size_t uartChannel, char c);
void uart_puts(size_t spiChannel, size_t uartChannel, const char* buf, size_t blen);
void* memcpy(void* restrict dest, const void* restrict src, size_t n);

char uart_read_register_wrapper(int channel, char reg);
void uart_write_register_wrapper(int channel, char reg, char data);

void enable_uart_interrupt(enum events event);
void disable_uart_interrupt(enum events event);
void init_gpio_interrupts();
enum events uart_interrupt_handler();
void clear_uart_interrupt();
void wait_for_train_read();


#endif