#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include "common.h"
#define PRIORITIES 6 //0-4 normal priorities, priority 5 for servers
long int createTask(long int priority, void(*function)());
struct task_descriptor* schedule();
void yieldTask();
void removeTask();
void setupScheduler();
long int nameServerCreate(long int priority, void(*function)());
void kernelSend(long int tid, const char *msg, long int msglen, char *reply, long int rplen);
void kernelReceive(long int *tid, char *msg, long int msglen);
void kernelReply(long int tid, const char *reply, long int rplen);
long int awaitEventTask(long int event);
long int receiveEvent(long int event);
long int isWaitingEvent(long int event);
extern struct task_descriptor taskBlock[NUMTASKS];

#endif