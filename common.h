#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>

#define terminal_channel  0
#define train_channel     1

//#define _KERNEL_DEBUG_
#define DEBUG_CHANNEL  terminal_channel
// Note: increase tick size before enabling _KERNEL_DEBUG_

//#define DEBUG_LOGS

#define NUMTASKS 500
#define MESSAGE_MAX_SIZE 1000
#define STACK_SIZE 2097152 //multiple of 8


#define IDLE_TASK_PRIORITY 0
// 1-4 are open for use
#define SERVER_PRIORITY 5



// uncomment if track A & comment if track B
#define IS_TRACKA

#define IS_BROKEN

//Delays in 10msec ticks
#define SWITCH_DELAY 20
#define REVERSE_DELAY 430

#define SWITCH_TIME_TOLERANCE 50
#define SWITCH_DIST_TOLERANCE 300
#define COLLISION_TIME_TOLERANCE 150
#define COLLISION_DIST_TOLERANCE 375

// _% out of 100%
#define TRAIN_CONTROL_DESYNC_TOLERANCE 20

#define TRANSMIT_BUFFER_SIZE (MESSAGE_MAX_SIZE * NUMTASKS)

#define SENSOR_TIMEOUT_PING   5
#define FIRST_SENSOR_TIMEOUT  20
#define SENSOR_TIMEOUT        4

#define NUMTRAINS 2
#define TRAIN1 24
#define TRAIN2 58

//Speeds in micrometers / tick
#define TRAIN1_SPEED14      6650
#define TRAIN1_SPEED12      5256
#define TRAIN1_SPEED10      3613
#define TRAIN1_SPEED8       2252
#define TRAIN_SPEED_DEFAULT 4000

#define TRAIN2_SPEED14      6900
#define TRAIN2_SPEED12      4890
#define TRAIN2_SPEED10      3288
#define TRAIN2_SPEED8       1966

//acceleration in nanometers / tick^2
#define TRAIN1_ACCELERATION_14      10968
#define TRAIN1_ACCELERATION_12      10737
#define TRAIN1_ACCELERATION_10      10036
#define TRAIN1_ACCELERATION_8       8171
#define TRAIN1_ACCELERATION_DEFAULT 10500
// /*
#define TRAIN1_DECELERATION_14      -16312
#define TRAIN1_DECELERATION_12      -15831
#define TRAIN1_DECELERATION_10      -13096
#define TRAIN1_DECELERATION_8       -10597
#define TRAIN1_DECELERATION_DEFAULT -14000 

#define TRAIN2_ACCELERATION_14      9523
#define TRAIN2_ACCELERATION_12      8716
#define TRAIN2_ACCELERATION_10      7289
#define TRAIN2_ACCELERATION_8       5714
#define TRAIN2_ACCELERATION_DEFAULT 8000
// /*
#define TRAIN2_DECELERATION_14      -17184
#define TRAIN2_DECELERATION_12      -14556
#define TRAIN2_DECELERATION_10      -12238
#define TRAIN2_DECELERATION_8       -9418
#define TRAIN2_DECELERATION_DEFAULT -13000


// alpha * 100 (0.15->15)
#define CALIBRATION_ALPHA 15


//timer.h for time related parameters

enum status {
  stat_SendWait = 0,
  stat_ReplyWait = 1,
  stat_ReceiveWait = 2,
  stat_EventWait = 3, 
  stat_Ready = 4
};

enum events {
  event_Clock = 0,
  event_Terminal_Receive = 1,
  event_Terminal_Transmit = 2,
  event_Train_Receive = 3,
  event_Train_Transmit = 4,
  event_none = 5
};
//update receiveEvent & awaitEvent if adding more events

//UART regs
#define UART_RHR          0x00
#define UART_THR          0x00
#define UART_TXLVL        0x08

struct task_descriptor{
    long int TID;
    long int parentTID;
    char *sp;
    char *replyBuff;
    long int replySz;
    char sendBuff[MESSAGE_MAX_SIZE];
    long int sendSz;
    long int sendTid;
    char *receiveBuff;
    long int receiveSz;
    long int *receiveTid;
    long int spsr;
    long int retval;
    long int p1;
    long int p2;
    long int p3;
    long int p4;
    long int p5;
    long int priority;
    void (*pc)();
    char mystack[STACK_SIZE];
    struct task_descriptor *next;
    struct task_descriptor *prev;
    enum status stat;
    int active;
    enum events awaitingEvent; 
};

enum request {
  req_Create = 0, //Need to pass 2 parameters along with it
  req_MyTid = 1,
  req_MyParentTid = 2,
  req_Yield = 3,
  req_Exit = 4,
  req_Send = 5,
  req_Receive = 6,
  req_Reply = 7,
  req_Interrupt = 8,
  req_AwaitEvent = 9,
  req_Shutdown = 10,
  req_UartWriteRegister = 11,
  req_UartReadRegister = 12,
  req_kernel_print = 13, //for debugging/testing only!!!!
  req_train_getc = 14
};


enum print_type {
  print_log = 0,
  print_time = 1,
  print_idle = 2,
  print_prev_input = 3,
  print_timeouts = 4,
  print_debug = 5,
  print_input = 6,
  print_input_help = 7,
  print_seperators = 8,
  print_sensor_i = 9,
  //... sensor_5 = 14,
  print_switch_i = 15,
  //... switch_18 = 32,
  //switch_153 = 33,
  //... switch_156 = 36,
  print_train1_speed = 37,
  print_train_diff = 38,
  print_path_i = 39,
  //path_2 = 40
}; 
//print_type must be a char (0<=type<=255) or else change Print sys_call wrapper

extern struct task_descriptor *curr;
extern int waiting;

int parseNum(char *str, int* pos, int* success);

long int sqrt(long int n);

#endif
