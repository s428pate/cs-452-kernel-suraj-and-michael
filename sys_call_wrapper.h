#ifndef _SYS_CALL_WRAPPER_H_
#define _SYS_CALL_WRAPPER_H_

#include "common.h"

//
//  For Userspace to call
//




//  Assumes Name server created right after first task

long int RegisterAs(const char *name);
/*
registers the task id of the caller under the given name. On return without error 
it is guaranteed that all WhoIs() calls by any task will return the task id of the 
caller until the registration is overwritten. If another task has already 
registered with the given name, its registration is overwritten. RegisterAs() is 
actually a wrapper covering a send to the name server.

Return Value:
    0	    success.
    -1	    invalid name server task id inside wrapper. 


Assumptions:
    name ends with '\0'
*/

long int WhoIs(const char *name);
/*
    asks the name server for the task id of the task that is registered under the 
    given name. Whether WhoIs() blocks waiting for a registration or returns with 
    an error, if no task is registered under the given name, is 
    implementation-dependent. There is guaranteed to be a unique task id associated 
    with each registered name, but the registered task may change at any time after 
    a call to WhoIs(). WhoIs() is actually a wrapper covering a send to the name 
    server.

    Return Value:
    tid	    task id of the registered task.
    -1	    invalid name server task id inside wrapper. 
*/



long int Time(long int tid);
/*
returns the number of ticks since the clock server was created and initialized. With a 10 millisecond tick and a 32-bit unsigned int for the time wraparound is almost 12,000 hours, plenty of time for your demo. Time is actually a wrapper for a send to the clock server. The argument is the tid of the clock server.
Return Value
>-1	time in ticks since the clock server initialized.
-1	tid is not a valid clock server task.
*/

long int Delay(long int tid, int ticks);
/*
returns after the given number of ticks has elapsed. How long after is not guaranteed because the caller may have to wait on higher priority tasks. Delay() is (almost) identical to Yield() if ticks is zero. Delay() is actually a wrapper for a send to the clock server.
Return Value
>-1	success. The current time returned (as in Time())
-1	tid is not a valid clock server task.
-2	negative delay.
*/

long int DelayUntil(long int tid, int ticks);
/*
returns when the time since clock server initialization is greater or equal than the given number of ticks. How long after is not guaranteed because the caller may have to wait on higher priority tasks. Also, DelayUntil(tid, Time(tid) + ticks) may differ from Delay(tid, ticks) by a small amount. DelayUntil is actually a wrapper for a send to the clock server.
Return Value
>-1	success. The current time returned (as in Time())
-1	tid is not a valid clock server task.
-2	negative delay.
*/




int Getc(long int tid, int uart);
/*
returns next unreturned character from the given UART. The first argument is the task id of the appropriate server. How communication errors are handled is implementation-dependent. Getc() is actually a wrapper for a send to the appropriate server.

Return Value
>=0	new character from the given UART.
-1	tid is not a valid uart server task. 
*/

int Putc(long int tid, int uart, char ch);
/*
queues the given character for transmission by the given UART. On return the only guarantee is that the character has been queued. Whether it has been transmitted or received is not guaranteed. How communication errors are handled is implementation-dependent. Putc() is actually a wrapper for a send to the appropriate server.

Return Value
0	success.
-1	tid is not a valid uart server task. 
*/


//Terminal Interface

void Print(char * msg, long int val1, long int val2, long int val3, long int type);
/*
Sends print info to TerminalRT

Assumes msg is null terminated
*/

void init_print();
/* not a syscall: called by terminal server to initialise some static variables*/

//Train Interface

int TrainSpeed(long int train, long int speed);
/*
Sets Train Speeds

Return Value
0   success
-1  value out of range or any other errors
*/

int TrainReverse(long int train);
/*
Creates Task that Reverses the Train

Return Value
0   success
-1  value out of range or any other errors
*/

int TrainSwitch(long int turnout, char direction);
/*
Sets Switch Direction

Return Value
0   success
-1  value out of range or any other errors
*/


void pure_train_reverse(int train);
#endif
